<?php

/* ---------------------------------------------------------------------------
 * Child Theme URI | DO NOT CHANGE
 * --------------------------------------------------------------------------- */
define( 'CHILD_THEME_URI', get_stylesheet_directory_uri() );


/* ---------------------------------------------------------------------------
 * Define | YOU CAN CHANGE THESE
 * --------------------------------------------------------------------------- */

// White Label --------------------------------------------
define( 'WHITE_LABEL', true );

// Static CSS is placed in Child Theme directory ----------
define( 'STATIC_IN_CHILD', true );


/* ---------------------------------------------------------------------------
 * Enqueue Style
 * --------------------------------------------------------------------------- */
add_action( 'wp_enqueue_scripts', 'mfnch_enqueue_styles', 101 );

function mfnch_enqueue_styles() {

	// Enqueue the parent rtl stylesheet
	if ( is_rtl() ) {
		wp_enqueue_style( 'mfn-rtl', get_template_directory_uri() . '/rtl.css' );
	}

	// Enqueue the child stylesheet
	wp_dequeue_style( 'style' );
	wp_enqueue_style( 'style', CHILD_THEME_URI .'/assets/style/style.css' );

	// Enqueue javascript
	wp_enqueue_script( 'lazyload-script', CHILD_THEME_URI .'/assets/script/lazyload.js', array(), false, true );
	wp_enqueue_script( 'iframe-script', CHILD_THEME_URI .'/assets/script/iframe.js', array(), false, false );
	wp_enqueue_script( 'custom-script', CHILD_THEME_URI .'/assets/script/custom.js', array(), false, true );

	if ( is_page_template( 'infographic.php' ) ) {
		wp_enqueue_style( 'infographic-css', CHILD_THEME_URI .'/assets/css/infographic.css' );
	}

	if ( is_single() ) {
		wp_enqueue_style( 'arvo-font', 'https://fonts.googleapis.com/css?family=Arvo:700' );
	}
}


/* ---------------------------------------------------------------------------
 * I HATE MODERN TRIBE
 * --------------------------------------------------------------------------- */
add_filter( 'tribe_events_google_maps_api', 'remove_tec_google_maps_api_key' );
add_filter( 'tribe_events_pro_google_maps_api', 'remove_tec_google_maps_api_key' );

function remove_tec_google_maps_api_key() {

	if ( ! empty( $api_key ) ) {
		return '';
	}
}


/* ---------------------------------------------------------------------------
 * Load Textdomain
 * --------------------------------------------------------------------------- */
add_action( 'after_setup_theme', 'mfnch_textdomain' );

function mfnch_textdomain() {
	load_child_theme_textdomain( 'betheme',  get_stylesheet_directory() . '/languages' );
	load_child_theme_textdomain( 'mfn-opts', get_stylesheet_directory() . '/languages' );
}


/* ---------------------------------------------------------------------------
 * Override theme functions
 * --------------------------------------------------------------------------- */
$includes = [
	'lib/theme-shortcodes.php',
	'lib/theme-functions.php',
	'lib/theme-head.php',
	'lib/theme-admin.php',
	'includes/content-post.php'
];

foreach ($includes as $file) {
	if (!$filepath = locate_template($file)) {
	trigger_error(sprintf(__('Error locating %s for inclusion %s', 'betheme'), $file, $filepath), E_USER_ERROR);
	}

	require_once $filepath;
}
unset($file, $filepath);


/* ---------------------------------------------------------------------------
 * Kill wp emoji
 * --------------------------------------------------------------------------- */
function disable_emojis() {
	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
	remove_action( 'wp_print_styles', 'print_emoji_styles' );
	remove_action( 'admin_print_styles', 'print_emoji_styles' );
	remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
	remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
	remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
	add_filter( 'tiny_mce_plugins', 'disable_emojis_tinymce' );
	add_filter( 'wp_resource_hints', 'disable_emojis_remove_dns_prefetch', 10, 2 );
}

add_action( 'init', 'disable_emojis' );

/**
* Filter function used to remove the tinymce emoji plugin.
*
* @param array $plugins
* @return array Difference betwen the two arrays
*/
function disable_emojis_tinymce( $plugins ) {
	if ( is_array( $plugins ) ) {
		return array_diff( $plugins, array( 'wpemoji' ) );
	} else {
		return array();
	}
}

/**
* Remove emoji CDN hostname from DNS prefetching hints.
*
* @param array $urls URLs to print for resource hints.
* @param string $relation_type The relation type the URLs are printed for.
* @return array Difference betwen the two arrays.
*/
function disable_emojis_remove_dns_prefetch( $urls, $relation_type ) {
	if ( 'dns-prefetch' == $relation_type ) {
		/** This filter is documented in wp-includes/formatting.php */
		$emoji_svg_url = apply_filters( 'emoji_svg_url', 'https://s.w.org/images/core/emoji/2/svg/' );

		$urls = array_diff( $urls, array( $emoji_svg_url ) );
	}
	return $urls;
}


/* ---------------------------------------------------------------------------
 * Remove Location post type archive
 * --------------------------------------------------------------------------- */
function remove_archive_locations_post_type( $args, $post_type ) {
    if ( $post_type == "bh_sl_locations" ) {
        $args['publicly_queryable'] = false;
        $args['has_archive'] = false;
    }

    return $args;
}
add_filter( 'register_post_type_args', 'remove_archive_locations_post_type', 20, 2 );


/* ---------------------------------------------------------------------------
 * Alter Cardinal Location GMaps URL
 * Adding CA region and FR language for French
 * --------------------------------------------------------------------------- */
function bh_sl_modify_gmap_url( $url ) {
	if ( get_bloginfo( 'language' ) == 'fr-CA' ) {
		return $url . '&language=fr&region=CA';
	} else {
		return $url . '&region=CA';
	}
}
add_filter( 'bh_sl_gmap', 'bh_sl_modify_gmap_url' );
