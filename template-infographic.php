<?php
/**
 * Template Name: Infographic
 *
 * @package Recycle My Electronics
 * @author Resonator
 */


?>
<!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
    <link href="https://fonts.googleapis.com/css?family=Arvo:700|Montserrat|Open+Sans" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo CHILD_THEME_URI; ?>/assets/css/infographic.css">
  </head>
  <body>
    <!-- infographic wrapper -->
    <div class="navigation">
      <a href="#" id="go-left"> < </a>
      <a href="#" id="go-right"> > </a>

      <button class="back-btn">Exit</button>
    </div>

    <!-- circuit texture -->
    <div class="texture">
      <img src="<?php echo CHILD_THEME_URI; ?>/assets/images/infographic/circuittexture.png" alt="">
    </div>

    <div class="infographic-wrapper">
      <!-- Page 0: Front page -->
      <div class="front-page" id="">
        <div class="bg-mask"></div>
        <img class="logo" src="<?php echo CHILD_THEME_URI; ?>/assets/images/logo_vertical.png" alt="Recycle My Eletronics Logo" />
        <div class="tagline">
          <p>The future is in your hands. </p>
          <p>Don't let it go to waste.&trade;</p>
        </div>
      </div>

      <!-- Page 1 -->
      <div class="steps" id="step1">
        <div class="step-wrapper">
          <image class="img-bubble" id="step1-img" src="<?php echo CHILD_THEME_URI; ?>/assets/images/infographic/step1.png" />

          <!-- path 1 -->
          <svg id="path1" class="path" xmlns="http://www.w3.org/2000/svg" width="100" height="150" viewBox="0 0 100 150">
              <defs>
                <mask id="theMask1p">
                  <rect id="theSquare1p" x="0" y="-150" width="100" height="150" fill="#fff" />
                </mask>
              </defs>
              <g id="toBeRevealed1p" mask="url(#theMask1p)">
              	<polyline points="30,0 30,72 80,72 80,150" style="fill:none;stroke:#fff;stroke-width:22" />
              </g>
          </svg>

          <!-- info-bubble -->
          <svg id="info1" class="info-bubble" xmlns="http://www.w3.org/2000/svg" width="300" height="300" viewBox="0 0 100 100">
            <defs>
              <mask id="theMask1b">
                <rect id="theSquare1b" x="0" y="-100" width="100" height="100" fill="#fff" />
              </mask>
            </defs>
            <g id="toBeRevealed1b" mask="url(#theMask1b)">
              <circle cx="50" cy="50" r="40" fill="#06402e" stroke="#fff" stroke-width="6"  />
              <text y="30" transform="translate(50)"  fill="#fff" >
                <tspan x="0" text-anchor="middle">Since the</tspan>
                <tspan x="0" dy="1.5em" text-anchor="middle" >program began,</tspan>
                <tspan x="0" dy="1.5em" text-anchor="middle" >Recycle My Electronics</tspan>
                <tspan x="0" dy="1.5em" text-anchor="middle" >has diverted approximately</tspan>
                <tspan x="0" dy="1.5em" text-anchor="middle" font-weight="bold">100 million devices</tspan>
                <tspan x="0" dy="1.5em" text-anchor="middle" >from landfill and</tspan>
                <tspan x="0" dy="1.5em" text-anchor="middle" >illegal export.</tspan>
              </text>
            </g>
          </svg>

          <!-- path 1-2 -->
          <svg id="path1-2" class="path" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 300 300">
            <defs>
              <mask id="theMask1-2p">
                <rect id="theSquare1-2p" x="-300" y="0" width="300" height="300" fill="#fff" />
              </mask>
            </defs>
            <g id="toBeRevealed1-2p" mask="url(#theMask1-2p)">
            	<polyline points="0,30 120,30 120,100 300,290" style="fill:none;stroke:#fff;stroke-width:22"
            </g>
          </svg>

        </div>

      </div>

      <!-- Page 2 -->
      <div class="steps" id="step2">
        <div class="step-wrapper">
          <!-- step 2 image -->
          <img class="img-bubble" id="step2-img" src="<?php echo CHILD_THEME_URI; ?>/assets/images/infographic/step2.png" />

          <!-- step 2 path 1 -->
          <svg id="path2-1" class="path" xmlns="http://www.w3.org/2000/svg" width="100" height="100" viewBox="0 0 100 100">
              <defs>
                <mask id="theMask2-1p">
                  <rect id="theSquare2-1p" x="0" y="100" width="100" height="100" fill="#fff" />
                </mask>
              </defs>
              <g id="toBeRevealed2-1p" mask="url(#theMask2-1p)">
              	<polyline points="20,0 20,50 80,100" style="fill:none;stroke:#fff;stroke-width:15"
              </g>
          </svg>

          <!-- step2 info-bubble -->
          <svg id="info2" class="info-bubble" xmlns="http://www.w3.org/2000/svg" width="300" height="300" viewBox="0 0 100 100">
            <defs>
              <mask id="theMask2b">
                <rect id="theSquare2b" x="0" y="100" width="100" height="100" fill="#fff" />
              </mask>
            </defs>
            <g id="toBeRevealed2b" mask="url(#theMask2b)">
              <circle cx="50" cy="50" r="40" fill="#06402e" stroke="#fff" stroke-width="6"  />
              <text y="30" transform="translate(50)"  fill="#fff" >
                <tspan x="0" text-anchor="middle">To find out</tspan>
                <tspan x="0" dy="1.5em" text-anchor="middle">what and where</tspan>
                <tspan x="0" dy="1.5em" text-anchor="middle">to recycle, visit:</tspan>
                <tspan x="0" dy="1.5em" text-anchor="middle">recycleMYelectronics.ca</tspan>
              </text>
            </g>
          </svg>

          <!-- step2 path2-3 -->
          <svg id="path2-3" class="path" xmlns="http://www.w3.org/2000/svg" width="300" height="300" viewBox="0 0 300 300">
            <defs>
              <mask id="theMask2-3p">
                <rect id="theSquare2-3p" x="-300" y="300" width="300" height="300" fill="#fff" />
              </mask>
            </defs>
            <g id="toBeRevealed2-3p" mask="url(#theMask2-3p)">
              <circle cx="100" cy="100" r="15" fill="none" stroke="#fff" stroke-width="16" />
              <polyline points="110,110 150,150" style="fill:none;stroke:#fff;stroke-width:17" />
            	<polyline points="0,300 300,0" style="fill:none;stroke:#fff;stroke-width:22" />
            </g>
          </svg>
        </div>

      </div>

      <!-- Page 3 -->
      <div class="steps" id="step3">
        <div class="step-wrapper">
          <!-- step 2 image -->
          <img class="img-bubble" id="step3-img"  src="<?php echo CHILD_THEME_URI; ?>/assets/images/infographic/step3.png" />

          <!-- step 3 path 1 -->
          <svg id="path3-1" class="path" xmlns="http://www.w3.org/2000/svg" width="100" height="100" viewBox="0 0 100 100">
              <defs>
                <mask id="theMask3-1p">
                  <rect id="theSquare3-1p" x="0" y="-100" width="100" height="100" fill="#fff" />
                </mask>
              </defs>
              <g id="toBeRevealed3-1p" mask="url(#theMask3-1p)">
              	<path d="M 30 0 l -20 38 M 10 30 l 70 80" fill="none" stroke="#fff" stroke-width="17px" />
              </g>
          </svg>

          <!-- step3 info-bubble1 -->
          <svg id="info3" class="info-bubble" xmlns="http://www.w3.org/2000/svg" width="300" height="300" viewBox="0 0 100 100">
            <defs>
              <mask id="theMask3b">
                <rect id="theSquare3b" x="0" y="-100" width="100" height="100" fill="#fff" />
              </mask>
            </defs>
            <g id="toBeRevealed3b" mask="url(#theMask3b)">
              <circle cx="50" cy="50" r="40" fill="#06402e" stroke="#fff" stroke-width="6"  />
              <text y="30" transform="translate(50)"  fill="#fff" >
                <tspan x="0" text-anchor="middle">DROP-OFF</tspan>
                <tspan x="0" dy="1.5em" text-anchor="middle">End-of-life electronics</tspan>
                <tspan x="0" dy="1.5em" text-anchor="middle">can be dropped off at:</tspan>
                <tspan x="0" dy="1.5em" text-anchor="middle">- Recycle My Electronics</tspan>
                <tspan x="0" dy="1.5em" text-anchor="middle">drop-off locations</tspan>
                <tspan x="0" dy="1.5em" text-anchor="middle">- Return to retail locations</tspan>
                <tspan x="0" dy="1.5em" text-anchor="middle">- Special collection </tspan>
                <tspan x="0" dy="1.5em" text-anchor="middle">event</tspan>
              </text>
            </g>
          </svg>

          <!-- step3 path3-2 -->
          <svg id="path3-2" class="path" xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="0 0 100 100">
            <defs>
              <mask id="theMask3-2">
                <rect id="theSquare3-2p" x="-100" y="0" width="100" height="100" fill="#fff" />
              </mask>
            </defs>
            <g id="toBeRevealed3-2" mask="url(#theMask3-2)">
              <rect x="0" y="0" width="100" height="50"  fill="#fff"  />
            </g>
          </svg>

          <!-- step3 info2 -->
          <svg id="info3-2" class="info-bubble" xmlns="http://www.w3.org/2000/svg" width="300" height="300" viewBox="0 0 100 100">
            <defs>
              <mask id="theMask3-2b">
                <rect id="theSquare3-2b" x="-100" y="0" width="100" height="100" fill="#fff" />
              </mask>
            </defs>
            <g id="toBeRevealed3-2b" mask="url(#theMask3-2b)">
              <circle cx="50" cy="50" r="45" fill="#06402e" stroke="#fff" stroke-width="4" />
              <text y="25" transform="translate(50)"  fill="#fff" >
                <tspan x="0" text-anchor="middle">As a recognized</tspan>
                <tspan x="0" dy="1.5em" text-anchor="middle">industry-led,</tspan>
                <tspan x="0" dy="1.5em" text-anchor="middle">not-for-profit organization,</tspan>
                <tspan x="0" dy="1.5em" text-anchor="middle">Recycle My Electronics</tspan>
                <tspan x="0" dy="1.5em" text-anchor="middle">programs provide</tspan>
                <tspan x="0" dy="1.5em" text-anchor="middle">environmental compliance</tspan>
                <tspan x="0" dy="1.5em" text-anchor="middle">programs for  manufacturers,</tspan>
                <tspan x="0" dy="1.5em" text-anchor="middle">distributors and retailers</tspan>
                <tspan x="0" dy="1.5em" text-anchor="middle">of electronics.</tspan>
              </text>
            </g>
          </svg>
        </div>
      </div>

      <!-- Page 4 -->
      <div class="steps" id="step4">
        <div class="step-wrapper">
          <!-- step4 image -->
          <img class="img-bubble" id="step4-img" src="<?php echo CHILD_THEME_URI; ?>/assets/images/infographic/step4.png" />

          <!-- step4 path 1 -->
          <svg id="path4" class="path" xmlns="http://www.w3.org/2000/svg" width="250" height="300" viewBox="0 0 250 300">
              <defs>
                <mask id="theMask4p">
                  <rect id="theSquare4p" x="0" y="-550" width="250" height="300" fill="#fff" />
                </mask>
              </defs>
              <g id="toBeRevealed4p" mask="url(#theMask4p)">
              	<polyline points="100,0 100,200 230,280" style="fill:none;stroke:#fff;stroke-width:17" />
                <polyline points="100,40 50,100 50,200" style="fill:none;stroke:#fff;stroke-width:17" />
                <circle cx="50" cy="220" r="15" fill="none" stroke="#fff" stroke-width="16"  />
              </g>
          </svg>

          <!-- step4 info-bubble1 -->
          <svg id="info4" class="info-bubble" xmlns="http://www.w3.org/2000/svg" width="300" height="300" viewBox="0 0 100 100">
            <defs>
              <mask id="theMask4b">
                <rect id="theSquare4b" x="0" y="100" width="100" height="100" fill="#fff" />
              </mask>
            </defs>
            <g id="toBeRevealed4b" mask="url(#theMask4b)">
              <circle cx="50" cy="50" r="40" fill="#06402e" stroke="#fff" stroke-width="4"  />
              <text y="30" transform="translate(50)"  fill="#fff" >
                <tspan x="0" text-anchor="middle">TRANSPORT</tspan>
                <tspan x="0" dy="1.5em" text-anchor="middle">Materials in remote</tspan>
                <tspan x="0" dy="1.5em" text-anchor="middle">locations are transported</tspan>
                <tspan x="0" dy="1.5em" text-anchor="middle">to consolidation centres</tspan>
                <tspan x="0" dy="1.5em" text-anchor="middle">for efficiency and to</tspan>
                <tspan x="0" dy="1.5em" text-anchor="middle">reduce the carbon</tspan>
                <tspan x="0" dy="1.5em" text-anchor="middle">footprint.</tspan>
              </text>
            </g>
          </svg>

          <!-- step4 path4-1 -->
          <svg id="path4-1" class="path" xmlns="http://www.w3.org/2000/svg" width="120" height="120" viewBox="0 0 120 120">
            <defs>
              <mask id="theMask4-1p">
                <rect id="theSquare4-1p" x="0" y="150" width="120" height="120" fill="#fff" />
              </mask>
            </defs>
            <g id="toBeRevealed4-1p" mask="url(#theMask4-1p)">
              <polyline points="15,15 80,50" style="fill:none;stroke:#fff;stroke-width:17" />
              <polyline points="70,50 45,90" style="fill:none;stroke:#fff;stroke-width:17" />
            </g>
          </svg>
        </div>
      </div>

      <!-- Page 5 -->
      <div class="steps" id="step5">
        <div class="step-wrapper">
          <!-- step4 path4-2 -->
          <svg id="path4-2" class="path" xmlns="http://www.w3.org/2000/svg" width="280" height="100" viewBox="0 0 280 100">
              <defs>
                <mask id="theMask4-2p">
                  <rect id="theSquare4-2p" x="-280" y="0" width="280" height="100" fill="#fff" />
                </mask>
              </defs>
              <g id="toBeRevealed4-2p" mask="url(#theMask4-2p)">
                <polyline points="0,50 50,80 270,80 270,20" style="fill:none;stroke:#fff;stroke-width:22" />
              </g>
          </svg>

          <!-- step5 image -->
          <img class="img-bubble" id="step5-img" src="<?php echo CHILD_THEME_URI; ?>/assets/images/infographic/step5.png" />

          <!-- step5 info-bubble1 -->
          <svg id="info5" class="info-bubble" xmlns="http://www.w3.org/2000/svg" width="300" height="300" viewBox="0 0 100 100">
            <defs>
              <mask id="theMask5b">
                <rect id="theSquare5b" x="0" y="100" width="100" height="100" fill="#fff" />
              </mask>
            </defs>
            <g id="toBeRevealed5b" mask="url(#theMask5b)">
              <circle cx="50" cy="50" r="40" fill="#06402e" stroke="#fff" stroke-width="5"  />
              <text y="30" transform="translate(50)"  fill="#fff" >
                <tspan x="0" text-anchor="middle">Recycle My</tspan>
                <tspan x="0" dy="1.5em" text-anchor="middle">Electronics programs</tspan>
                <tspan x="0" dy="1.5em" text-anchor="middle">only work with verified</tspan>
                <tspan x="0" dy="1.5em" text-anchor="middle">recyclers ensuring that</tspan>
                <tspan x="0" dy="1.5em" text-anchor="middle">end-of-life electronics </tspan>
                <tspan x="0" dy="1.5em" text-anchor="middle">and environmentally</tspan>
                <tspan x="0" dy="1.5em" text-anchor="middle">sound manner.</tspan>
              </text>
            </g>
          </svg>

          <!-- step 5 path 1 -->
          <svg id="path5-1" class="path" xmlns="http://www.w3.org/2000/svg" width="200" height="200" viewBox="0 0 200 200">
              <defs>
                <mask id="theMask5-1p">
                  <rect id="theSquare5-1p" x="0" y="200" width="200" height="200" fill="#fff" />
                </mask>
              </defs>
              <g id="toBeRevealed5-1p" mask="url(#theMask5-1p)">
              	<path d="M 100 0 l -80 100 M 20 90 l 70 80" fill="none" stroke="#fff" stroke-width="17px"></path>
              </g>
          </svg>
        </div>
      </div>

      <!-- Page 6 -->
      <div class="steps" id="step6">

        <div class="step-wrapper">
          <svg id="path6" class="path" xmlns="http://www.w3.org/2000/svg" width="250" height="380" viewBox="0 0 250 380">
              <defs>
                <mask id="theMask6p">
                  <rect id="theSquare6p" x="0" y="-380" width="250" height="380" fill="#fff" />
                </mask>
              </defs>
              <g id="toBeRevealed6p" mask="url(#theMask6p)">
              	<polyline points="0,20 50,20 50,200 250,380" style="fill:none;stroke:#fff;stroke-width:22" />
              </g>
          </svg>

          <!-- step6 image -->
          <img class="img-bubble" id="step6-img"  src="<?php echo CHILD_THEME_URI; ?>/assets/images/infographic/step6.png" />

          <!-- step6 path6-1 -->
          <svg id="path6-1" class="path" xmlns="http://www.w3.org/2000/svg" width="30" height="100" viewBox="0 0 30 100">
            <defs>
              <mask id="theMask6-1p">
                <rect id="theSquare6-1p" x="0" y="100" width="100" height="100" fill="#fff" />
              </mask>
            </defs>
            <g id="toBeRevealed6-1p" mask="url(#theMask6-1p)">
              <rect x="0" y="0" width="30" height="100"  fill="#fff"  />
            </g>
          </svg>

          <!-- step6 info-bubble1 -->
          <svg id="info6" class="info-bubble" xmlns="http://www.w3.org/2000/svg" width="300" height="300" viewBox="0 0 100 100">
            <defs>
              <mask id="theMask6b">
                <rect id="theSquare6b" x="0" y="100" width="100" height="100" fill="#fff" />
              </mask>
            </defs>
            <g id="toBeRevealed6b" mask="url(#theMask6b)">
              <circle cx="50" cy="50" r="40" fill="#06402e" stroke="#fff" stroke-width="4"  />
              <text y="35" transform="translate(50)"  fill="#fff" >
                <tspan x="0" text-anchor="middle">Recyclers are</tspan>
                <tspan x="0" dy="1.5em" text-anchor="middle">prohibited from</tspan>
                <tspan x="0" dy="1.5em" text-anchor="middle">exporting electronics</tspan>
                <tspan x="0" dy="1.5em" text-anchor="middle">or substances of</tspan>
                <tspan x="0" dy="1.5em" text-anchor="middle">concern to non-OECD</tspan>
                <tspan x="0" dy="1.5em" text-anchor="middle">nations.</tspan>
              </text>
            </g>
          </svg>

          <!-- step6 path7 -->
          <svg id="path7" class="path" xmlns="http://www.w3.org/2000/svg" width="300" height="100" viewBox="0 0 300 100">
            <defs>
              <mask id="theMask7p">
                <rect id="theSquare7p" x="-300" y="0" width="300" height="100" fill="#fff" />
              </mask>
            </defs>
            <g id="toBeRevealed7p" mask="url(#theMask7p)">
              <polyline points="0,50 50,80 280,80 280,20" style="fill:none;stroke:#fff;stroke-width:20" />
            </g>
          </svg>
        </div>

      </div>

      <!-- Page 7 -->
      <div class="steps" id="step7">
        <div class="step-wrapper">
          <!-- step7 image -->
          <img class="img-bubble" id="step7-img" src="<?php echo CHILD_THEME_URI; ?>/assets/images/infographic/step7.png" />

          <!-- step7 info-bubble1 -->
          <svg id="info7" class="info-bubble" xmlns="http://www.w3.org/2000/svg" width="300" height="300" viewBox="0 0 100 100">
            <defs>
              <mask id="theMask7b">
                <rect id="theSquare7b" x="0" y="100" width="100" height="100" fill="#fff" />
              </mask>
            </defs>
            <g id="toBeRevealed7b" mask="url(#theMask7b)">
              <circle cx="50" cy="50" r="40" fill="#06402e" stroke="#fff" stroke-width="4"  />
              <text y="30" transform="translate(50)"  fill="#fff" >
                <tspan x="0" text-anchor="middle">With 40% more</tspan>
                <tspan x="0" dy="1.5em" text-anchor="middle">Recycle My Electronics</tspan>
                <tspan x="0" dy="1.5em" text-anchor="middle">drop-off locations across</tspan>
                <tspan x="0" dy="1.5em" text-anchor="middle">the country, recycling</tspan>
                <tspan x="0" dy="1.5em" text-anchor="middle">your end-of-life</tspan>
                <tspan x="0" dy="1.5em" text-anchor="middle">electronics has never</tspan>
                <tspan x="0" dy="1.5em" text-anchor="middle">been easier.</tspan>
              </text>
            </g>
          </svg>

          <!-- path8 -->
          <svg id="path8" class="path" xmlns="http://www.w3.org/2000/svg" width="380" height="250" viewBox="0 0 380 250">
            <defs>
              <mask id="theMask8p">
                <rect id="theSquare8p" x="-380" y="250" width="380" height="250" fill="#fff" />
              </mask>
            </defs>
            <g id="toBeRevealed8p" mask="url(#theMask8p)">
              <polyline points="0,250 380,0" style="fill:none;stroke:#fff;stroke-width:20" />
            </g>
          </svg>
        </div>
      </div>

      <!-- Page 8 -->
      <div class="steps" id="step8">
        <div class="step-wrapper">
          <!-- step8 image -->
          <img class="img-bubble" id="step8-img" src="<?php echo CHILD_THEME_URI; ?>/assets/images/infographic/step8.png" />

          <!-- step8 info-bubble1 -->
          <svg id="info8" class="info-bubble" xmlns="http://www.w3.org/2000/svg" width="300" height="300" viewBox="0 0 100 100">
            <defs>
              <mask id="theMask8b">
                <rect id="theSquare8b" x="0" y="-100" width="100" height="100" fill="#fff" />
              </mask>
            </defs>
            <g id="toBeRevealed8b" mask="url(#theMask8b)">
              <circle cx="50" cy="50" r="40" fill="#06402e" stroke="#fff" stroke-width="4"  />
              <text y="30" transform="translate(50)"  fill="#fff" >
                <tspan x="0" text-anchor="middle">Recycle My</tspan>
                <tspan x="0" dy="1.5em" text-anchor="middle">Electronics programs</tspan>
                <tspan x="0" dy="1.5em" text-anchor="middle">require that all recyclers</tspan>
                <tspan x="0" dy="1.5em" text-anchor="middle">actively process material</tspan>
                <tspan x="0" dy="1.5em" text-anchor="middle">and maintain appropriate</tspan>
                <tspan x="0" dy="1.5em" text-anchor="middle">environmental, health,</tspan>
                <tspan x="0" dy="1.5em" text-anchor="middle">safety and security</tspan>
                <tspan x="0" dy="1.5em" text-anchor="middle">controls.</tspan>
              </text>
            </g>
          </svg>

          <!-- step 8 path 1 -->
          <svg id="path8-1" class="path" xmlns="http://www.w3.org/2000/svg" width="120" height="120" viewBox="0 0 120 120">
              <defs>
                <mask id="theMask8-1p">
                  <rect id="theSquare8-1p" x="0" y="-120" width="120" height="120" fill="#fff" />
                </mask>
              </defs>
              <g id="toBeRevealed8-1p" mask="url(#theMask8-1p)">
              	<polyline points="15,15 80,50" style="fill:none;stroke:#fff;stroke-width:17"></polyline>
                <polyline points="70,50 45,90" style="fill:none;stroke:#fff;stroke-width:17"></polyline>
              </g>
          </svg>

          <!-- path9 -->
          <svg id="path9" class="path" xmlns="http://www.w3.org/2000/svg" width="250" height="20" viewBox="0 0 250 15">
            <defs>
              <mask id="theMask9p">
                <rect id="theSquare9p" x="-250" y="0" width="100%" height="100%" fill="#fff" />
              </mask>
            </defs>
            <g id="toBeRevealed9p" mask="url(#theMask9p)">
              <rect x="0" y="0" width="100%" height="100%" fill="#fff"  />
            </g>
          </svg>

          <!-- step9 path9-1 -->
          <svg id="path9-1" class="path" xmlns="http://www.w3.org/2000/svg" width="100" height="100" viewBox="0 0 100 100">
              <defs>
                <mask id="theMask9-1p">
                  <rect id="theSquare9-1p" x="0" y="-100" width="100" height="100" fill="#fff" />
                </mask>
              </defs>
              <g id="toBeRevealed9-1p" mask="url(#theMask9-1p)">
                <polyline points="50,0 50,70" style="fill:none;stroke:#fff;stroke-width:10" />
                <circle cx="50" cy="80" r="13" fill="none" stroke="#fff" stroke-width="6" />

              </g>
          </svg>
        </div>
      </div>

      <!-- Page 9 -->
      <div class="steps" id="step9">
        <div class="step-wrapper">
          <!-- step9 image -->
          <image class="img-bubble" id="step9-img" src="<?php echo CHILD_THEME_URI; ?>/assets/images/infographic/step9.png" />

          <!-- step9 material image1 -->
          <svg class="img-bubble material-img" id="step9-img1" xmlns="http://www.w3.org/2000/svg" width="14" height="50" viewBox="0 0 14 50">
            <defs>
              <mask id="theMask9-1">
                <rect id="theSquare9-1" x="0" y="-50" width="14" height="50" fill="#fff" />
              </mask>
            </defs>
            <g id="toBeRevealed9-1" mask="url(#theMask9-1)">
              <rect x="0" y="0" width="14" height="50"  fill="#fff"  />
            </g>
          </svg>

          <div class="material-wrapper" id="material-wrapper1">
            <img src="<?php echo CHILD_THEME_URI; ?>/assets/images/infographic/material1.png" />
            <div class="material-desc">
              <p>METALS - </p>
              <p>SUCH AS FERROUS,</p>
              <p>ALUMINUM</p>
              <p>AND COPPER</p>
            </div>
          </div>

          <!-- step9 material image2 -->
          <svg class="img-bubble material-img" id="step9-img2" xmlns="http://www.w3.org/2000/svg" width="14" height="50" viewBox="0 0 14 50">
            <defs>
              <mask id="theMask9-2">
                <rect id="theSquare9-2" x="0" y="-50" width="14" height="50" fill="#fff" />
              </mask>
            </defs>
            <g id="toBeRevealed9-2" mask="url(#theMask9-2)">
              <rect x="0" y="0" width="14" height="50"  fill="#fff"  />
            </g>
          </svg>

          <div class="material-wrapper" id="material-wrapper2">
            <img src="<?php echo CHILD_THEME_URI; ?>/assets/images/infographic/material2.png" />
            <div class="material-desc">
              <p>OTHER PRECIOUS</p>
              <p>METALS -</p>
              <p>SUCH AS SILVER,</p>
              <p>COPPER AND GOLD</p>
            </div>
          </div>

          <!-- step9 material image3 -->
          <svg class="img-bubble material-img" id="step9-img3" xmlns="http://www.w3.org/2000/svg" width="14" height="50" viewBox="0 0 14 50">
            <defs>
              <mask id="theMask9-3">
                <rect id="theSquare9-3" x="0" y="-50" width="14" height="50" fill="#fff" />
              </mask>
            </defs>
            <g id="toBeRevealed9-3" mask="url(#theMask9-3)">
              <rect x="0" y="0" width="14" height="50"  fill="#fff"  />
            </g>
          </svg>

          <div class="material-wrapper" id="material-wrapper3">
            <img src="<?php echo CHILD_THEME_URI; ?>/assets/images/infographic/material3.png" />
            <div class="material-desc">
              <p>CABLES</p>
              <p>AND WIRES -</p>
              <p>CONTAINING PLASTICS</p>
              <p>AND COPPER</p>
            </div>
          </div>

          <!-- step9 material image4 -->
          <svg class="img-bubble material-img" id="step9-img4" xmlns="http://www.w3.org/2000/svg" width="14" height="40" viewBox="0 0 14 50">
            <defs>
              <mask id="theMask9-4">
                <rect id="theSquare9-4" x="0" y="-50" width="14" height="50" fill="#fff" />
              </mask>
            </defs>
            <g id="toBeRevealed9-4" mask="url(#theMask9-4)">
              <rect x="0" y="0" width="14" height="50"  fill="#fff"  />
            </g>
          </svg>

          <div class="material-wrapper" id="material-wrapper4">
            <img src="<?php echo CHILD_THEME_URI; ?>/assets/images/infographic/material4.png" />
            <div class="material-desc">
              <p>GLASS</p>
            </div>
          </div>

          <!-- step9 material image5 -->
          <svg class="img-bubble material-img" id="step9-img5" xmlns="http://www.w3.org/2000/svg" width="14" height="50" viewBox="0 0 14 50">
            <defs>
              <mask id="theMask9-5">
                <rect id="theSquare9-5" x="0" y="-50" width="14" height="50" fill="#fff" />
              </mask>
            </defs>
            <g id="toBeRevealed9-5" mask="url(#theMask9-5)">
              <rect x="0" y="0" width="14" height="50"  fill="#fff"  />
            </g>
          </svg>

          <div class="material-wrapper" id="material-wrapper5">
            <img src="<?php echo CHILD_THEME_URI; ?>/assets/images/infographic/material5.png" />
            <div class="material-desc">
              <p>LEADED GLASS,</p>
              <p>CATHODE RAY TUBES</p>
              <p>(CRT), AND LEADED</p>
              <p>PLASMA DISPLAY</p>
            </div>
          </div>

          <!-- step 9 path9-2 -->
          <svg id="path9-2" class="path" xmlns="http://www.w3.org/2000/svg" width="1000" height="200" viewBox="0 0 1000 200">
              <defs>
                <mask id="theMask9-2p">
                  <rect id="theSquare9-2p" x="-1000" y="0" width="100%" height="100%" fill="#fff" />
                </mask>
              </defs>
              <g id="toBeRevealed9-2p" mask="url(#theMask9-2p)">
                <polyline points="0,90 90,170 250,170 500,20 1200,20" style="fill:none;stroke:#fff;stroke-width:12" />
              </g>
          </svg>

          <!-- step 9 path9-3 -->
          <svg id="path9-3" class="path" xmlns="http://www.w3.org/2000/svg" width="120" height="250" viewBox="0 0 120 250">
              <defs>
                <mask id="theMask9-3p">
                  <rect id="theSquare9-3p" x="0" y="250" width="100%" height="100%" fill="#fff" />
                </mask>
              </defs>
              <g id="toBeRevealed9-3p" mask="url(#theMask9-3p)">
              	<polyline points="10,250 10,150 120,0" style="fill:none;stroke:#fff;stroke-width:12" />
              </g>
          </svg>

          <!-- step9 path9-4 -->
          <svg id="path9-4" class="path" xmlns="http://www.w3.org/2000/svg" width="14" height="50" viewBox="0 0 14 50">
            <defs>
              <mask id="theMask9-4p">
                <rect id="theSquare9-4p" x="0" y="-50" width="100%" height="100%" fill="#fff" />
              </mask>
            </defs>
            <g id="toBeRevealed9-4p" mask="url(#theMask9-4p)">
              <rect x="0" y="0" width="100%" height="100%"  fill="#fff"  />
            </g>
          </svg>

          <!-- step9 path9-5 -->
          <svg id="path9-5" class="path" xmlns="http://www.w3.org/2000/svg" width="670" height="14" viewBox="0 0 670 14">
            <defs>
              <mask id="theMask9-5p">
                <rect id="theSquare9-5p" x="0" y="-14" width="100%" height="100%" fill="#fff" />
              </mask>
            </defs>
            <g id="toBeRevealed9-5p" mask="url(#theMask9-5p)">
              <rect x="0" y="0" width="100%" height="100%"  fill="#fff"  />
            </g>
          </svg>

          <!-- step9 info-bubble1 -->
          <svg id="info9" class="info-bubble" xmlns="http://www.w3.org/2000/svg" width="300" height="300" viewBox="0 0 100 100">
            <defs>
              <mask id="theMask9b">
                <rect id="theSquare9b" x="0" y="100" width="100" height="100" fill="#fff" />
              </mask>
            </defs>
            <g id="toBeRevealed9b" mask="url(#theMask9b)">
              <circle cx="50" cy="50" r="35" fill="#06402e" stroke="#fff" stroke-width="4"  />
              <text y="40" transform="translate(50)"  fill="#fff" >
                <tspan x="0" text-anchor="middle">Materials move</tspan>
                <tspan x="0" dy="1.5em" text-anchor="middle">through additional</tspan>
                <tspan x="0" dy="1.5em" text-anchor="middle">points of security</tspan>
                <tspan x="0" dy="1.5em" text-anchor="middle">resources and protect </tspan>
                <tspan x="0" dy="1.5em" text-anchor="middle">the environment.</tspan>
              </text>
            </g>
          </svg>
        </div>
      </div>

      <div class="steps" id="step10">
        <div class="step-wrapper">
          <!-- step9 image -->
          <img class="img-bubble" id="step10-img" src="<?php echo CHILD_THEME_URI; ?>/assets/images/infographic/btn-save.png" />

          <div class="tagline">
            <p>The future is in your hands. </p>
            <p>Don't let it go to waste.&trade;</p>
            <p>
              <img class="logo" src="<?php echo CHILD_THEME_URI; ?>/assets/images/logo_horizontal.png" alt="Recycle My Eletronics Logo" />
            </p>

          </div>
        </div>
    </div>
    <script src="//code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/gsap/1.20.4/TweenMax.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/ScrollMagic.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/plugins/animation.gsap.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/plugins/debug.addIndicators.js"></script>
    <script>

      if ( getUrlParameter("fullscreen") === '1' ) {
        $('.navigation .back-btn').show();
      };

      $('.navigation .back-btn').click(function () {
        history.back(1);
      });

      // init controller
      var controller = new ScrollMagic.Controller({vertical: false});

      // each step:
      $('.steps').each(function(index) {

        var tween = new TimelineMax();
        var offset = 0;

        if ( index == 0 ) {
         // build tween
         tween.to("#step1-img", 0.05, {autoAlpha:1})
              .to("#theSquare1p", 0.03, {attr:{y:0}}, "-=0.05")
              .to("#theSquare1b", 0.05, {attr:{y:0}}, "-=0.01")
              .to("#theSquare1-2p", 0.03, {attr:{x:0}}, "-=0.02");
        } else if ( index == 1 ) {
          offset = -100;
          tween.to("#step2-img", 0.03, {autoAlpha:1}, "-0.05")
               .to("#theSquare2-1p", 0.05, {attr:{y:0}}, "-=0.04")
               .to("#theSquare2b", 0.07, {attr:{y:0}}, "-=0.03")
               .to("#theSquare2-3p", 0.03, {attr:{x:0, y:0}},"-=0.05")
        } else if ( index == 2 ) {
          offset = -150;
          tween.to("#step3-img", 0.02, {autoAlpha:1}, "-0.01")
               .to("#theSquare3-1p", 0.01, {attr:{y:0}}, "-=0.01")
               .to("#theSquare3b", 0.02, {attr:{y:0}}, "-=0.005")
               .to("#theSquare3-2p", 0.01, {attr:{x:0}}, "-=0.01")
               .to("#theSquare3-2b", 0.02, {attr:{x:0}}, "-=0.005")
        } else if ( index == 3 ) {
          offset = -150;
          tween.to("#theSquare4p", 0.005, {attr:{y:0}}, "-0.01")
               .to("#step4-img", 0.01, {autoAlpha:1}, "-=0.007")
               .to("#theSquare4-1p", 0.003, {attr:{y:0}}, "-=0.005")
               .to("#theSquare4b", 0.01, {attr:{y:0}}, "-=0.003")
        } else if ( index == 4 ) {
          offset = -100;
          tween.to("#theSquare4-2p", 0.005, {attr:{x:0}}, "-0.01")
               .to("#theSquare5b", 0.01, {attr:{y:0}}, "-=0.007")
               .to("#theSquare5-1p", 0.003, {attr:{y:0}}, "-=0.005")
               .to("#step5-img", 0.01, {autoAlpha:1}, "-=0.002")
        } else if ( index == 5 ) {
          offset = -100;
          tween.to("#theSquare6p", 0.005, {attr:{y:0}}, "-0.01")
               .to("#step6-img", 0.01, {autoAlpha:1}, "-=0.008")
               .to("#theSquare6-1p", 0.005, {attr:{y:0}}, "-=0.007")
               .to("#theSquare6b", 0.005, {attr:{y:0}}, "-=0.007")
               .to("#theSquare7p", 0.008, {attr:{x:0}}, "-=0.00")
        } else if ( index == 6 ) {
          offset = -100;
          tween.to("#step7-img", 0.006, {autoAlpha:1}, "-=0.008")
               .to("#theSquare7b", 0.005, {attr:{y:0}}, "-=0.009")
               .to("#theSquare8p", 0.005, {attr:{x:0,y:0}}, "-=0.005")
        } else if ( index == 7 ) {
          offset = -150;
          tween.to("#step8-img", 0.01, {autoAlpha:1})
               .to("#theSquare8-1p", 0.01, {attr:{y:0}}, "-=0.003")
               .to("#theSquare8b", 0.01, {attr:{y:0}}, "-=0.003")
               .to("#theSquare9p", 0.01, {attr:{x:0}}, "-=0.005")
               .to("#theSquare9-1p", 0.005, {attr:{y:0}}, "-=0.006")
        } else if ( index == 8 ) {
          offset = -150;
          tween.to("#step9-img", 0.1, {autoAlpha:1})
               .to("#theSquare9-2p", 0.5, {attr:{x:0}}, "-=0.08")
               .to("#theSquare9-3p", 0.05, {attr:{y:0}}, "-=0.45")
               .to("#theSquare9-4p", 0.1, {attr:{y:0}}, "-=0.45")
               .to("#theSquare9b", 0.1, {attr:{y:0}}, "-=0.45")
               .to("#theSquare9-5p", 0.01, {attr:{y:0}}, "-=0.371")
               .to(".material-img mask rect", 0.01, {attr:{y:0}}, "-=0.35")
               .to(".material-wrapper", 0.05, {autoAlpha:1}, "-=0.35")
               .to("#step10-img", 0.05, {autoAlpha:1})
               .to("#step10 .tagline", 0.05, {left:0},  "-=0.05")
        } else if ( index == 9 ) {
          offset = -1*(window.innerWidth/2);
          tween.to("#step10", 0.51, {backgroundPosition: "-250px 0px", ease: Linear.easeNone})
        }

        // build scene
        var duration = $(this).width() + Math.abs(offset);
        var scene = new ScrollMagic.Scene({triggerElement:this, duration:duration, offset:offset})
                .setTween(tween)
                // .addIndicators() //TODO: indicators TO BE REMOVED
                .addTo(controller);

        var triggerHook = scene.triggerHook();
        // set a new triggerHook using a number
        scene.triggerHook(0.5);
      });


      // set material images width
      var line_left = $('#path9-5').css("left").slice(0, -2);
      var first_img_left = parseInt(line_left);
      var first_content_left = first_img_left - 70;
      $('.material-img').each(function(index) {
        var left_value = first_img_left + index*165;
        var left_value_unit = left_value + 'px';
        $(this).css("left", left_value_unit);
      });
      $('.material-wrapper').each(function(index) {
        var left_value = first_content_left + index*165;
        var left_value_unit = left_value + 'px';
        $(this).css("left", left_value_unit);
      });

      // navigation btn
      var timeout;

      var isTouchDevice = 'ontouchstart' in document.documentElement;

      var leftPos = $('html, body').scrollLeft();
      function scroll_window(direction) {
        var i = 0;
        timeout = setInterval(function(){
          if ( direction === "r" ) {
            leftPos += 20;
          } else if ( direction === "l" ) {
            leftPos -= 20;
          }
          $('html, body').animate({scrollLeft: leftPos}, 10);
        }, 50);
      }

      function clear_timeout() {
        clearInterval(timeout);
      }



      $('#go-left, #go-right').click(function(e){
        e.preventDefault();
      });

      $('#go-right').mousedown(function(e){
         scroll_window("r");
      }).mouseup(function(e){
        clearInterval(timeout);
      }).on('touchstart', function(){
         if (isTouchDevice){
           scroll_window("r");
         }
      }).on('touchend', function(){
        if (isTouchDevice){
          clearInterval(timeout);
        }
      });

      $('#go-left').mousedown(function(e){
        scroll_window("l");
      }).mouseup(function(e){
        clearInterval(timeout);
      }).on('touchstart', function(){
         if (isTouchDevice){
           scroll_window("l");
         }
      }).on('touchend', function(){
        if (isTouchDevice){
          clearInterval(timeout);
        }
      });

      function getUrlParameter(name) {
        name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
        var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
        var results = regex.exec(location.search);
        return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
      };

      var clicked = false, clickX;
      $(document).on({
          'mousemove': function(e) {
              clicked && updateScrollPos(e);
          },
          'mousedown': function(e) {
              clicked = true;
              clickX = e.pageX;
          },
          'mouseup': function() {
              clicked = false;
              $('html').css('cursor', 'ew-resize');
          }
      });

      var updateScrollPos = function(e) {
          $('html').css('cursor', 'ew-resize');
          $(window).scrollLeft($(window).scrollLeft() + (clickX - e.pageX));
      }
    </script>

  </body>

</html>
