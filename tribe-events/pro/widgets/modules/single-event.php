<?php
/**
 * Single Event Template for Widgets
 *
 * This template is used to render single events for both the calendar widget and advanced
 * list widget
 *
 * 2016-05-13 -- https://gist.github.com/cliffordp/9222f36879ddfb37fb5687c9d5c4382d
 * This file belongs here: /wp-content/themes/YOUR_CHILD_THEME/tribe-events/pro/widgets/modules/single-event.php
 * It overrides version 4.1.2's /wp-content/plugins/events-calendar-pro/src/views/pro/widgets/modules/single-event.php
 * Result: http://cl.ly/291z0Y3t2g1Z
 *
 */
$mini_cal_event_atts = tribe_events_get_widget_event_atts();
$postDate = tribe_events_get_widget_event_post_date();
$organizer_ids = tribe_get_organizer_ids();
$multiple_organizers = count( $organizer_ids ) > 1;
// Setup an array of venue details for use later in the template
$venue_details = tribe_get_venue_details();

// Venue
$has_venue_address = ( ! empty( $venue_details['address'] ) ) ? ' location' : '';

?>

<div class="tribe-mini-calendar-event tribe_test event-<?php esc_attr_e( $mini_cal_event_atts['current_post'] ); ?> <?php esc_attr_e( $mini_cal_event_atts['class'] ); ?>">
	<?php echo tribe_event_featured_image( null, 'medium' ); ?>
	<div class="list-date">
		<span class="month"><?php echo tribe_get_start_date(null, false, 'M') ?></span><span class="day"><?php echo tribe_get_start_date(null, false, 'j') ?></span><span class="week"><?php echo tribe_get_start_date(null, false, 'D') ?></span>
	</div>

	<div class="list-info">
		<?php do_action( 'tribe_events_list_widget_before_the_event_title' ); ?>

		<h2 class="tribe-events-title">
			<a href="<?php echo esc_url( tribe_get_event_link() ); ?>" rel="bookmark"><?php the_title(); ?></a>
		</h2>

		<?php do_action( 'tribe_events_list_widget_after_the_event_title' ); ?>

		<?php do_action( 'tribe_events_list_widget_before_the_meta' ) ?>

		<div class="tribe-events-duration">
			<?php echo tribe_events_event_schedule_details(); ?>
		</div>


		<div class="tribe-events-location ">
			<?php echo $venue_details['address']; ?>
		</div>

		<?php do_action( 'tribe_events_list_widget_after_the_meta' ) ?>

	</div> <!-- .list-info -->

	<a href="<?php echo esc_url( tribe_get_event_link() ); ?>" class="tribe-events-read-more" rel="bookmark"> <?php echo do_shortcode('[icon type="icon-newspaper"]') ?> <?php esc_html_e( 'Read more', 'the-events-calendar' ) ?> &raquo;</a>

</div>
