<?php 
global $wpsl_settings, $wpsl;

if ( isset( $_POST['wpsl-widget-radius'] ) ) {
    $radius = absint( $_POST['wpsl-widget-radius'] );

    $radius_dropdown = str_replace( array( '[', ']' ), '', $wpsl_settings['search_radius'] );
    $radius_dropdown = str_replace( $radius, "[$radius]", $radius_dropdown );

    $wpsl_settings['search_radius'] = $radius_dropdown;
}

$output         = $this->get_custom_css(); 
$autoload_class = ( !$wpsl_settings['autoload'] ) ? 'class="wpsl-not-loaded"' : '';

$output .= '<div id="wpsl-wrap">' . "\r\n";
$output .= "\t" . '<div class="wpsl-search wpsl-clearfix ' . $this->get_css_classes() . '">' . "\r\n";
$output .= "\t\t" . '<div id="wpsl-title-wrap">' . "\r\n";
$output .= "\t\t\t" . '<span class="location-search-title">' . esc_html__( 'Find Drop-off Locations', 'wpsl' ) . '</span>' . "\r\n";
$output .= "\t\t" . '</div>' . "\r\n";
$output .= "\t\t" . '<div id="wpsl-search-wrap">' . "\r\n";
$output .= "\t\t\t" . '<form autocomplete="off">' . "\r\n";

$output .= "\t\t\t" . '<div class="wpsl-input">' . "\r\n";
$output .= "\t\t\t\t" . '<div id="wpsl-radius">' . "\r\n";
$output .= "\t\t\t\t\t" . '<label for="wpsl-radius-dropdown">' . esc_html( $wpsl->i18n->get_translation( 'radius_label', __( 'Search radius', 'wpsl' ) ) ) . '</label>' . "\r\n";
$output .= "\t\t\t\t\t" . '<select id="wpsl-radius-dropdown" class="wpsl-dropdown" name="wpsl-radius">' . "\r\n";
$output .= "\t\t\t\t\t\t" . $this->get_dropdown_list( 'search_radius' ) . "\r\n";
$output .= "\t\t\t\t\t" . '</select>' . "\r\n";
$output .= "\t\t\t\t" . '</div>' . "\r\n";
$output .= "\t\t\t\t" . '<label for="wpsl-search-input">' . esc_html( $wpsl->i18n->get_translation( 'search_label', __( 'Your location', 'wpsl' ) ) ) . '</label>' . "\r\n";
$output .= "\t\t\t\t" . '<input id="wpsl-search-input" type="text" value="' . apply_filters( 'wpsl_search_input', '' ) . '" name="wpsl-search-input" placeholder="" aria-required="true" />' . "\r\n";
$output .= "\t\t\t\t" . '<div class="wpsl-my-location wpsl-icon-direction" title="' . esc_html__( 'My Location', 'wpsl' ) . '">' . esc_html__( 'My Location', 'wpsl' ) . '</div>';
$output .= "\t\t\t" . '</div>' . "\r\n";

$output .= "\t\t\t" . '<div id="wpsl-cat-filter">' . "\r\n";
if ( $this->use_category_filter() ) {
    $output .= $this->create_category_filter();
}
$output .= "\t\t\t" . '</div>' . "\r\n";

$output .= "\t\t\t\t" . '<div class="wpsl-search-btn-wrap"><input id="wpsl-search-btn" type="submit" value="' . esc_attr( $wpsl->i18n->get_translation( 'search_btn_label', __( 'Search', 'wpsl' ) ) ) . '"></div>' . "\r\n";

$output .= "\t\t" . '</form>' . "\r\n";
$output .= "\t\t" . '</div>' . "\r\n";
$output .= "\t" . '</div>' . "\r\n";
    
$output .= "\t" . '<div id="wpsl-gmap" class="wpsl-gmap-canvas"></div>' . "\r\n";

$output .= "\t" . '<div id="wpsl-result-list">' . "\r\n";
$output .= "\t\t" . '<div id="wpsl-stores" '. $autoload_class .'>' . "\r\n";
$output .= "\t\t\t" . '<ul>' . "\r\n";;
$output .= "\t\t\t\t" . '<li class="locator-call-to-action">' . "\r\n";
$output .= "\t\t\t\t\t" . '<h5> ' . __( 'Use our locator to find a drop-off location near you!', 'wpsl' ) . '</h5>';
$output .= "\t\t\t\t" . '</li>' . "\r\n";
$output .= "\t\t\t" . '</ul>' . "\r\n";
$output .= "\t\t" . '</div>' . "\r\n";
$output .= "\t\t" . '<div id="wpsl-direction-details">' . "\r\n";
$output .= "\t\t\t" . '<ul></ul>' . "\r\n";
$output .= "\t\t" . '</div>' . "\r\n";
$output .= "\t" . '</div>' . "\r\n";

$output .= '</div>' . "\r\n";

return $output;