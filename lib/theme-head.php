<?php
function mfn_sidebar_classes( $has_both = false )
{
	$classes = $both = false;
	
	if( mfn_ID() ){
		
		if( get_post_type() == 'page' && mfn_opts_get('single-page-layout') ){
			
			// Theme Options | Single - Page
			$layout = mfn_opts_get('single-page-layout');
									
		} elseif( get_post_type() == 'post' && is_single() && mfn_opts_get('single-layout') ){
			
			// Theme Options | Single - Post
			$layout = mfn_opts_get('single-layout');
			
		} elseif( get_post_type() == 'newsandmedia' && is_single() && mfn_opts_get('single-layout') ){
			
			// Cameron Nursall - add sidebar to CPT newsandmedia
			$layout = mfn_opts_get('single-layout');

		} elseif( get_post_type() == 'video' && is_single() && mfn_opts_get('single-layout') ){
			
			// Cameron Nursall - add sidebar to CPT video
			$layout = mfn_opts_get('single-layout');

		} elseif( get_post_type() == 'news' && is_single() && mfn_opts_get('single-layout') ){
			
			// Cameron Nursall - add sidebar to CPT news
			$layout = mfn_opts_get('single-layout');

		} elseif( get_post_type() == 'portfolio' && is_single() && mfn_opts_get('single-portfolio-layout') ){
			
			// Theme Options | Single - Portfolio
			$layout = mfn_opts_get('single-portfolio-layout');
								
		} else {
			
			// Post Meta
			$layout = get_post_meta( mfn_ID(), 'mfn-post-layout', true);
				
		}

		switch ( $layout ) {
			
			case 'left-sidebar':
				$classes = ' with_aside aside_left';
				break;
				
			case 'right-sidebar':
				$classes = ' with_aside aside_right';
				break;
				
			case 'both-sidebars':
				$classes = ' with_aside aside_both';
				$both = true;
				break;
				
		}
		
		// demo
		if( $_GET && key_exists( 'mfn-s', $_GET ) ){
			if( $_GET['mfn-s'] ){
				$classes = ' with_aside aside_right';
			} else {
				$classes = false;
			}
		}
	}

	
	// WooCommerce
	if( function_exists( 'is_woocommerce' ) ){
		
		if( is_woocommerce() ){
		
			if( ! isset( $layout ) || ! $layout ){
				
				// BeTheme version < 6.4 | DO NOT DELETE
				if( is_active_sidebar( 'shop' ) ) $classes = ' with_aside aside_right';
				
			} elseif( $layout == 'both-sidebars' ){
				
				// Only one sidebar for shop
				$classes = ' with_aside aside_right';
				
			}

		}
		
		if( function_exists( 'is_product' ) && is_product() && mfn_opts_get( 'shop-sidebar' ) == 'shop' ){
			$classes = false;
		}
		
	}


	// bbPress
	if( function_exists('is_bbpress') && is_bbpress() && is_active_sidebar( 'forum' ) ){
		$classes = ' with_aside aside_right';
	}
	
	// BuddyPress
	if( function_exists('is_buddypress') && is_buddypress() && is_active_sidebar( 'buddy' ) ){
		$classes = ' with_aside aside_right';
	}
	
	// Easy Digital Downloads
	if( ( get_post_type() == 'download' )  && is_active_sidebar( 'edd' ) ){
		$classes = ' with_aside aside_right';
	}
	
	// Events Calendar
	if( function_exists('tribe_is_month') && is_active_sidebar( 'events' ) ){
		if( tribe_is_month() || tribe_is_day() || tribe_is_event() || tribe_is_event_query() || tribe_is_venue() ){
			$classes = ' with_aside aside_right';
		}
	}
	
	
	// Page | Search
	if( is_search() ){
		if( is_active_sidebar( 'mfn-search' ) ){
			$classes = ' with_aside aside_right';
		} else {
			$classes = false;
		}
		
	}
	
	// Page | Blank Page, Under Construction
	if( is_page_template( 'template-blank.php' ) || is_page_template( 'under-construction.php' ) ){
		$classes = false;
	}

	
	// check if has both sidebars
	if( $has_both ) return $both;
	
	return $classes;
}