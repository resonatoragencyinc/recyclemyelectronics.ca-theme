<?php

// ------------------------------------------------------------------
// Add all your sections, fields and settings during admin_init
// ------------------------------------------------------------------
//

function settings_api_init() {
	// Add the section to reading settings so we can add our
	// fields to it
	add_settings_section(
	'locator_page_section',
	'Locator Page Settings',
	'setting_section_callback_function',
	'reading'
);
	
	// Add the field with the names and function to use for our new
	// settings, put it in our new section
	add_settings_field(
	'locator_page',
	'Locator Page',
	'setting_callback_function',
	'reading',
	'locator_page_section'
);
	
	// Register our setting so that $_POST handling is done for us and
	// our callback function just has to echo the <input>
	register_setting( 'reading', 'locator_page' );
} // settings_api_init()

add_action( 'admin_init', 'settings_api_init' );


// ------------------------------------------------------------------
// Settings section callback function
// ------------------------------------------------------------------
//
// This function is needed if we added a new section. This function 
// will be run at the start of our section
//

function setting_section_callback_function() {
	echo '<p>Choose the page which the locator widget will go to</p>';
}

// ------------------------------------------------------------------
// Callback function for our example setting
// ------------------------------------------------------------------
//
// creates a checkbox true/false option. Other types are surely possible
//

function setting_callback_function() {
	?> <label for="locator_page"><?php printf( __( 'Locator page: %s' ), wp_dropdown_pages( array( 'name' => 'locator_page', 'echo' => 0, 'show_option_none' => __( '&mdash; Select &mdash;' ), 'option_none_value' => '0', 'selected' => get_option( 'locator_page' ) ) ) ); ?></label> <?php
}