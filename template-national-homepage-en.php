<?php
/**
 * Template Name: RME National Homepage English
 *
 * @package Recycle My Electronics
 * @author Resonator
 */
?><!DOCTYPE html>
<html class="no-js<?php echo mfn_user_os(); ?>" <?php language_attributes(); ?><?php mfn_tag_schema(); ?>>

<!-- head -->
<head>

<!-- meta -->
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<?php if( mfn_opts_get('responsive') ) echo '<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">'; ?>

<?php do_action('wp_seo'); ?>

<link rel="shortcut icon" href="<?php mfn_opts_show('favicon-img', '/favicon.ico'); ?>" type="image/x-icon" />

<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/css/homepage_style.css">

<!-- wp_head() -->
<?php wp_head(); ?>
</head>

<!-- body -->
<body <?php body_class( 'template-blank' ); ?>>

	<?php do_action( 'mfn_hook_top' ); ?>
	
	<?php do_action( 'mfn_hook_content_before' ); ?>

	<!-- #Content -->
	<div id="Content">
		<div class="content_wrapper clearfix">
	
			<!-- .sections_group -->
			<div class="sections_group">
				<div id="demowrap">
					<div class="text-left" style="max-width:800px;margin:0 auto;padding:1px 0;">
						<a href="https://www.recyclermeselectroniques.ca/"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/french.png" style="margin-left: 600px;"></a>
					</div>

					<div class="text-left" style="max-width:800px;margin:0 auto;padding:1px 0;">
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/EPRA_recycle_my_electronics_logo.png" style="align-content:center">

						<p>Electronic Products Recycling Association operates regulated recycling programs in nine provinces across Canada. Please select your province to find out more about electronics recycling near you.</p>
					</div>

					<ul id="rig">
						<li>
							<a class="rig-cell" href="<?php echo network_site_url('/bc/', 'https'); ?>" target="_blank" id="BC">
								<img class="rig-img" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/thumb_bc.jpg">
								<span class="rig-overlay"></span>
								<span class="rig-text">British Columbia</span>
							</a>
						</li>

						<li>
							<a class="rig-cell" href="<?php echo network_site_url('/sk/', 'https'); ?>" target="_blank" id="SK">
								<img class="rig-img" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/thumb_sk.jpg">
								<span class="rig-overlay"></span>
								<span class="rig-text">Saskatchewan</span>
							</a>
						</li>

						<li>
							<a class="rig-cell" href="<?php echo network_site_url('/mb/', 'https'); ?>" target="_blank" id="MB">
								<img class="rig-img" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/thumb_mb.jpg">
								<span class="rig-overlay"></span>
								<span class="rig-text">Manitoba</span>
							</a>
						</li>

					</ul>
					<ul id="rig">
						<li>
							<a class="rig-cell" href="<?php echo network_site_url('/on/', 'https'); ?>" target="_blank" id="ON">
								<img class="rig-img" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/thumb_on.jpg">
								<span class="rig-overlay"></span>
								<span class="rig-text">Ontario</span>
							</a>
						</li>

						<li>
							<a class="rig-cell" href="<?php echo network_site_url('/qc/', 'https'); ?>" target="_blank" id="QC">
								<img class="rig-img" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/thumb_qc.jpg">
								<span class="rig-overlay"></span>
								<span class="rig-text">Québec</span>
							</a>
						</li>

						<li>
							<a class="rig-cell" href="<?php echo network_site_url('/nb/', 'https'); ?>" target="_blank" id="NB">
								<img class="rig-img" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/thumb_nb.jpg">
								<span class="rig-overlay"></span>
								<span class="rig-text">New Brunswick</span>

							</a>
						</li>

					</ul>
					<ul id="rig">
						<li>
							<a class="rig-cell" href="<?php echo network_site_url('/ns/', 'https'); ?>" target="_blank" id="NS">
								<img class="rig-img" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/thumb_ns.jpg">
								<span class="rig-overlay"></span>
								<span class="rig-text">Nova Scotia</span>
							</a>
						</li>

						<li>
							<a class="rig-cell" href="<?php echo network_site_url('/pei/', 'https'); ?>" target="_blank" id="PEI">
								<img class="rig-img" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/thumb_pei.jpg">
								<span class="rig-overlay"></span>
								<span class="rig-text">Prince Edward Island</span>
							</a>
						</li>


						<li>
							<a class="rig-cell" href="<?php echo network_site_url('/nl/', 'https'); ?>" target="_blank" id="NL">
								<img class="rig-img" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/thumb_nfld.jpg">
								<span class="rig-overlay"></span>
								<span class="rig-text">Newfoundland and Labrador</span>
							</a>
						</li>

					</ul>
				</div>
			</div>
	
		</div>
	</div>
	
	<?php do_action( 'mfn_hook_content_after' ); ?>
	
	<?php do_action( 'mfn_hook_bottom' ); ?>

<!-- wp_footer() -->
<?php wp_footer(); ?>

</body>
</html>