<?php

add_shortcode( 'get_post',                      'get_posts_shortcode' );
add_shortcode( 'get_megabits', 					'get_megabits_shortcode' );
add_shortcode( 'set_sidebar', 					'set_sidebar_shortcode' );
add_shortcode( 'set_location_search',           'set_sidebar_shortcode' );
add_shortcode( 'set_calculator', 				'set_calculator_shortcode' );
add_shortcode( 'get_custom_post_type',          'get_custom_post_type_shortcode' );
add_shortcode( 'get_post_categories',           'get_post_categories_shortcode' );
add_shortcode( 'landing_box', 					'landing_box_shortcode' );
add_shortcode( 'map_legend',                    'map_legend_shortcode' );
add_shortcode( 'get_videos',                    'get_videos_shortcode' );
add_shortcode( 'get_news',                      'get_news_shortcode' );
add_shortcode( 'get_news_sidebar',              'get_news_sidebar_shortcode' );
add_shortcode( 'get_infographic',               'get_infographic_shortcode' );


/* ---------------------------------------------------------------------------
 * Get Posts post type [get_post]
* --------------------------------------------------------------------------- */
function get_posts_shortcode( $atts ) {
    // define attributes and their defaults
    extract( shortcode_atts( array(
        'type' 				=> 'post',
        'order' 			=> 'date',
        'posts' 			=> -1,
        'category' 			=> '',
        'posts_per_page' 	=> 9
    ), $atts ) );

    // define query parameters based on attributes
    $options = array(
        'category_name' 	=> $category,
        'posts_per_page' 	=> $posts_per_page,
        'has_password'      => false
    );
    $query = new WP_Query( $options );
    // run the loop based on the query

    $output = '<div class="blog_list">';
    if ( $query->have_posts() ) {
        while ( $query->have_posts() ) :
            $query->the_post();
            $output .= '<div class="news_box">';
                $output .= '<div class="img_container">';
                    $output .= get_the_post_thumbnail(get_the_id(), 'large');
                $output .= '</div>';
                $output .= '<div class="news_info">';
                    $output .= '<span>';
                        $output .= do_shortcode('[icon type="icon-clock"]') . get_the_date();
                    $output .= '</span>';
                $output .= '</div>';

                $output .= '<a href="' . get_permalink() . '"><h3>' . get_the_title() . '</h3></a>';
                $output .= '<p>' . wp_trim_words( get_the_content(), 45, '..' ) . '</p>';

                $output .= '<footer>';
                    $output .= '<a href="' . get_permalink() . '">' . do_shortcode('[icon type="icon-newspaper"]') . esc_html__( 'Read more', 'betheme') . '</a>' ;
                $output .= '</footer>';

            $output .= '</div>';
        endwhile;
    }
    $output .= '</div>';

    wp_reset_postdata();

    return $output;
}


/* ---------------------------------------------------------------------------
 * Get posts from Learning Hub [get_megabits]
* --------------------------------------------------------------------------- */
function get_megabits_shortcode( $atts ) {
    $a = shortcode_atts( array(
        'lang' => 'en'
    ), $atts );

    $output = '';

    if ( $a['lang'] === 'fr' ) {
        $rss = fetch_feed( 'https://www.recyclermeselectroniques.ca/learning/feed/' );
    } else {
        $rss = fetch_feed( 'https://www.recyclemyelectronics.ca/learning/feed/' );
    }

    if ( ! is_wp_error( $rss ) ) {
        $rss_items = $rss->get_items( 0, 6 );
        if ( $rss_items ) {
            $output .= '<div class="news_grid megabits_grid">';
                foreach ( $rss_items as $item ) :
                    $output .= '<div class="news_box megabits_box">';
                    if ( $enclosure = $item->get_enclosure() ) {
                        $output .= '<div class="img_container">';
                        $output .= '<img class="lazyload" data-src="' . $enclosure->get_thumbnail() . '" class="feed-thumb" />';
                        $output .= '</div>';
                    }
                        $output .= '<a href="' . $item->get_permalink() . '" target="_blank"><h3>' . $item->get_title() . '</h3></a>';

                        $output .= '<footer>';
                            $output .= '<a href="' . $item->get_permalink() . '" target="_blank">' . do_shortcode( '[icon type="icon-newspaper"]' ) . esc_html__( 'Read more', 'betheme') . '</a>' ;
                        $output .= '</footer>';
                    $output .= '</div>'; // grid box end
                endforeach;
                $output .= '</div>'; // grid end
        }
    } else {
        $output .= $rss->get_error_message();
    }

    return $output;
}


/* ---------------------------------------------------------------------------
 * Sidebar location search [set_sidebar] or [set_location_search]
* --------------------------------------------------------------------------- */
function set_sidebar_shortcode( $atts ) {
    extract( shortcode_atts( array(
        'distance' => '50,100,150,200,500'
    ), $atts ) );
    $distances = explode( ",", $distance );

    $output = '';

    $output .= '<div class="location-form-container"><form id="bh-sl-user-location" action="' . get_permalink( get_option( 'locator_page' ) ) . '"><div class="bh-sl-form-input"><div class="bh-sl-form-input-group"><label for="bh-sl-address-widget">';
    $output .= esc_html__( 'of', 'betheme');
    $output .= '</label><input placeholder="' . esc_html__( 'Postal Code or City', 'betheme') . '" type="text" id="bh-sl-address-widget"  name="bh-sl-address"></div><div class="bh-sl-form-input-group"><label for="bh-sl-maxdistance">';
    $output .= esc_html__( 'within', 'betheme');
    $output .= '</label><select id="bh-sl-maxdistance" name="bh-sl-maxdistance">';
    foreach ( $distances as $km ) {
        $output .= '<option value="' . $km . '">' . $km . ' km</option>';
    }
    $output .= '</select></div></div><button id="bh-sl-submit" type="submit">' . esc_html__( 'Search', 'betheme') . '</button></form></div>';
    return $output;

}


/* ---------------------------------------------------------------------------
 * Collection calculator [set_calculator]
* --------------------------------------------------------------------------- */
function set_calculator_shortcode( $atts ) {
    extract( shortcode_atts( array (
        'phones' => false
    ), $atts ) );

    $output = '';

    $output .= '<div class="calculator-wrap">';
        $output .= '<h3>' . esc_html__( 'COLLECTION CALCULATOR', 'betheme') . '</h3>';

        $output .= '<div class="calculator-input">';
            $output .= '<div class="laptop">';
                $output .= '<div class="image_wrapper">';
                    $output .= '<img src="'. get_stylesheet_directory_uri() . '/assets/images/icon_laptop.png" />';
                $output .= '</div>';
                $output .= '<label for="input_laptop">' . esc_html__( 'Laptops', 'betheme') . '</label>';
                $output .= '<input type="text" id="input_laptop" />';
            $output .= '</div>';

            $output .= '<div class="desktop">';
                $output .= '<img src="'. get_stylesheet_directory_uri() . '/assets/images/icon_desktop.png" />';
                $output .= '<label for="input_desktop">' . esc_html__( 'Desktop computers', 'betheme') . '</label>';
                $output .= '<input type="text" id="input_desktop" />';
            $output .= '</div>';

            $output .= '<div class="tv">';
                $output .= '<img src="'. get_stylesheet_directory_uri() . '/assets/images/icon_tv.png" />';
                $output .= '<label for="input_tv">' . esc_html__( 'CRT TVs', 'betheme') . '</label>';
                $output .= '<input type="text" id="input_tv" />';
            $output .= '</div>';

            $output .= '<div class="led">';
                $output .= '<img src="'. get_stylesheet_directory_uri() . '/assets/images/icon_led.png" />';
                $output .= '<label for="input_led">' . esc_html__( 'LCD/LED TVs', 'betheme') . '</label>';
                $output .= '<input type="text" id="input_led" />';
            $output .= '</div>';

            if ( $phones ) {
                $portable = esc_html__( 'Phones', 'betheme');
            } else {
                $portable = esc_html__( 'Portable devices', 'betheme');
            }

            $output .= '<div class="portable">';
                $output .= '<img src="'. get_stylesheet_directory_uri() . '/assets/images/icon_cellphone.png" />';
                $output .= '<label for="input_portable">' . $portable . '</label>';
                $output .= '<input type="text" id="input_portable" />';
            $output .= '</div>';
        $output .= '</div>';

        $output .= '<button>calculate</button>';

        $output .= '<div class="tonnage">';
            $output .= '<h4>tonnage:</h4>';
            $output .= '<div class="result-wrap">';
                $output .= '<span class="result"></span>';
            $output .= '</div>';
        $output .= '</div>';

        $output .= '<div class="impact">';
            $output .= '<h4>impact:</h4>';
            $output .= '<div class="result-wrap">';
                $output .= '<span class="result gold"><label>' . esc_html__( 'Gold', 'betheme') . ':</label></span>';
                $output .= '<span class="result silver"><label>' . esc_html__( 'Silver', 'betheme') . ':</label></span>';
                $output .= '<span class="result copper"><label>' . esc_html__( 'Copper', 'betheme') . ':</label></span>';
                $output .= '<span class="result palladium"><label>' . esc_html__( 'Palladium', 'betheme') . ':</label></span>';
            $output .= '</div>';
        $output .= '</div>';

    $output .= '</div>';

    return $output;
}


/* ---------------------------------------------------------------------------
 * [get_custom_post_type]
* --------------------------------------------------------------------------- */
function get_custom_post_type_shortcode( $atts ) {
    // define attributes and their defaults
    extract( shortcode_atts( array (
        'post_type'			=> 'newsandmedia',
        'order' 			=> 'date',
        'category' 			=> '',
        'posts_per_page' 	=> -1,
        'expend' 			=> ''
    ), $atts ) );

    // define query parameters based on attributes
    $options = array(
            'post_type' 		=> 'newsandmedia',
            'category_name' 	=> $category,
            'posts_per_page' 	=> $posts_per_page,
            'has_password'      => false
    );
    $query = new WP_Query( $options );
    // run the loop based on the query

    $output = '<div class="news-list ' . $expend . '">';
    if ( $query->have_posts() ) {
        while ( $query->have_posts() ) :
            $query->the_post();
            $output .= '<div class="news_box">';
            $output .= '<div class="img_container">';
            $output .= get_the_post_thumbnail(get_the_id(), 'large');
            $output .= '</div>';
            $output .= '<div class="news_info">';
            $output .= '</div>';

            $output .= '<a href="' . get_permalink() . '"><h3>' . get_the_title() . '</h3></a>';

            $output .= '<p>' . wp_trim_words( get_the_content(), 55, '..' ) . '</p>';

            $output .= '<footer>';
            $output .= '<a href="' . get_permalink() . '">' . do_shortcode('[icon type="icon-newspaper"]') . esc_html__( 'Read more', 'betheme') . '</a>' ;
            $output .= '</footer>';

            $output .= '</div>';
        endwhile;
    }
    $output .= '</div>';

    wp_reset_postdata();

    return $output;
}


/* ---------------------------------------------------------------------------
 * [get_post_categories]
* --------------------------------------------------------------------------- */
function get_post_categories_shortcode( $atts ) {
    $args = array(
        'type' => 'post'
    );

    $categories = get_categories( $args );

    $output = '<div class="category_list">';
    $output .= '<h2>' . esc_html__( 'Categories', 'betheme') . '</h2>';
    foreach ( $categories as $category ) {
        $output .= '<span>' . $category->name . '</span>';
    }

    $output .= '</div>';

    return $output;
}


/* ---------------------------------------------------------------------------
 * Landing page box [landing_box][/landing_box]
* --------------------------------------------------------------------------- */
function landing_box_shortcode( $atts, $content = null ) {
    $a = shortcode_atts( array(
        'title' => '',
        'image' => '',
        'link' => ''
    ), $atts );

    $output = '';

    $output .= '<div class="landing_box ';
        if ( ! empty( $a['image'] ) ) {
            $output .= 'has_image">';
            $output .= '<div class="img_container">';
                $output .= '<img src="' . $a['image'] . '" />';
            $output .= '</div>';
        } else {
            $output .= '">';
        }
        $output .= '<div class="info_container">';
            $output .= '<a href="' . $a['link'] . '"><h3>' . $a['title'] . '</h3></a>';
            $output .= '<p>' . $content . '</p>';

            $output .= '<footer>';
                $output .= '<a href="' . $a['link'] . '">' . do_shortcode('[icon type="icon-newspaper"]') . esc_html__('See more', 'betheme') . '</a>' ;
            $output .= '</footer>';
        $output .= '</div>';
    $output .= '</div>';
    return $output;
}


/* ---------------------------------------------------------------------------
 * Map legend [map_legend][pin]...[/map_legend]
* --------------------------------------------------------------------------- */
function map_legend_shortcode( $atts ) {
    $a = shortcode_atts( array(
        'pins'	=> ''
    ), $atts );

    $output = '';
    $terms = get_terms( 'wpsl_store_category' );

    $output .= '<div class="map-legend">';
    $output .= '<ul>';

    foreach ( $terms as $term ) {
        $output .= '<li>';
        $output .= '<div class="pin">';

        if ( function_exists( 'z_taxonomy_image_url' ) ) {
            $output .= '<span class="pin-image"><img src="' . z_taxonomy_image_url( $term->term_id ) . '"></span>';
        }

        $output .= '<h4 class="pin-name">' . esc_html( $term->name ) . '</h4>';
        if (strlen(esc_html($term->description)) > 0) {
          $output .= '<div class="pin-tooltip"><i class="icon-info-circled"></i>';        
            $output .= '<div class="pin-description">' . esc_html( $term->description ) . '</div>';
          $output .= '</div>';
        }
        $output .= '</div>'; // end .pin
        $output .= '</li>';
    }

    $output .= '</ul>';
    $output .= '</div>';

    return $output;
}


/* ---------------------------------------------------------------------------
 * Get Videos post type [get_videos]
* --------------------------------------------------------------------------- */
function get_videos_shortcode( $atts ) {
    $a = shortcode_atts( array(
        'bydate' 	=> 'false',
        'cat' 		=> '',
    ), $atts );

    // define query parameters based on attributes
    $options = array(
        'post_type'			=> 'video',
        'orderby'			=> 'date',
        'order'				=> 'DESC',
        'posts_per_page'	=> -1,
        'videocategory'		=> $a['cat'],
        'has_password'      => false
    );
    $query = new WP_Query( $options );
    // run the loop based on the query

    $output = '<div class="video_grid">';

    if ( $query->have_posts() ) {
        while ( $query->have_posts() ) :
            $query->the_post();

            $video_video_provider = get_post_meta( get_the_id(), 'video_video_provider', true );
            $video_video_id = get_post_meta( get_the_id(), 'video_video_id', true );
            $image = '';

            if ( $video_video_provider === 'video_vimeo' && @file_get_contents( "https://vimeo.com/api/oembed.json?url=https%3A//vimeo.com/$video_video_id", false, stream_context_create( $arrContextOptions ) ) === FALSE ) {
                continue;
            }

            if ( $a['bydate'] == 'true' ) {
                // Assign the year to a variable
                $year = get_the_date('F Y', '', '', FALSE);

                // If your year hasn't been echoed earlier in the loop, echo it now
                if ($year !== $year_check) {
                     $output .= "<h2 class='video_timestamp'>" . $year . "</h2>";
                }

                 // Now that your year has been printed, assign it to the $year_check variable
                $year_check = $year;
            }

            $output .= '<div class="video_box">';
                if ( $video_video_provider === 'video_vimeo' ) {
                    // get Vimeo
                    $url = 'https://vimeo.com/api/oembed.json?url=https%3A//vimeo.com/' . $video_video_id . '&height=720&byline=false&portrait=false&title=false';

                    $arrContextOptions=array(
                        'ssl' => array(
                            'verify_peer' 		=> false,
                            'verify_peer_name' 	=> false,
                        ),
                    );

                    $content = @file_get_contents( $url, false, stream_context_create( $arrContextOptions ) );

                    $video = json_decode( $content, true );

                    // getting thumbnail from api
                    foreach ( $video as $key => $value ) {
                        if ( $key === 'thumbnail_url' ) {
                            $image = $value;
                        }
                    }
                } else if ( $video_video_provider === 'video_youtube' ) {
                    $image = 'https://img.youtube.com/vi/' . $video_video_id . '/0.jpg';
                }

                $output .= '<a href="' . get_permalink() . '">';
                    $output .= '<div class="image_wrapper">';
                        $output .= '<img src="' . $image . '" />';
                    $output .= '</div>';
                    $output .= '<p>' . get_the_title() . '</p>';
                $output .= '</a>';

            $output .= '</div>';
        endwhile;
    }
    $output .= '</div>';

    wp_reset_postdata();

    return $output;
}


/* ---------------------------------------------------------------------------
 * FAQ [faq][faq_item]../[/faq]
 * --------------------------------------------------------------------------- */
function sc_faq( $attr, $content = null )
{
    extract(shortcode_atts(array(
        'title' 	=> '',
        'tabs' 		=> '',
        'open1st' 	=> '',
        'openall' 	=> '',
        'openAll' 	=> '',
    ), $attr));

    // class
    $class = '';
    if( $open1st ) $class .= ' open1st';
    if( $openall || $openAll ) $class .= ' openAll';

    $output  = '';

    $output .= '<div class="faq">';
        if( $title ) $output .= '<h4 class="title">'. $title .'</h4>';
        $output .= '<div class="mfn-acc faq_wrapper '. $class .'">';

            if( is_array( $tabs ) ){
                // content builder
                $i = 0;
                foreach( $tabs as $tab ){
                    $i++;
                    $output .= '<div class="question" itemscope itemtype="http://schema.org/Question">';
                        $output .= '<div class="title"><span class="num">'. $i .'</span><i class="icon-plus acc-icon-plus"></i><i class="icon-minus acc-icon-minus"></i><span itemprop="text">'. $tab['title'] .'</span></div>';
                        $output .= '<div class="answer" itemprop="acceptedAnswer" itemscope itemtype="http://schema.org/Answer">';
                            $output .= '<span itemprop="text">'. do_shortcode($tab['content']) .'</span>';
                        $output .= '</div>';
                    $output .= '</div>'."\n";
                }
            } else {
                // shortcode
                $output .= do_shortcode($content);
            }

        $output .= '</div>';
    $output .= '</div>'."\n";

    return $output;
}


/* ---------------------------------------------------------------------------
 * FAQ Item [faq_item][/faq_item]
* --------------------------------------------------------------------------- */
function sc_faq_item( $attr, $content = null )
{
    extract(shortcode_atts(array(
        'title' 	=> '',
        'number' 	=> '1',
    ), $attr));

    $output = '<div class="question" itemscope itemtype="http://schema.org/Question">';
        $output .= '<div class="title"><span class="num">'. $number .'</span><i class="icon-plus acc-icon-plus"></i><i class="icon-minus acc-icon-minus"></i>'. $title .'</div>';
        $output .= '<div class="answer" itemprop="acceptedAnswer" itemscope itemtype="http://schema.org/Answer">';
            $output .= '<p itemprop="text">'. do_shortcode( $content ) .'</p>';
        $output .= '</div>';
    $output .= '</div>'."\n";

    return $output;
}


/* ---------------------------------------------------------------------------
 * Get News post type [get_news]
* --------------------------------------------------------------------------- */
function get_news_shortcode( $atts ) {
    // define query parameters based on attributes
    $options = array(
        'post_type' 		=> 'news',
        'orderby' 			=> 'date',
        'order' 			=> 'DESC',
        'posts_per_page' 	=> -1,
        'has_password'      => false
    );
    $query = new WP_Query( $options );
    // run the loop based on the query

    $output = '<div class="news_grid2">';

    if ( $query->have_posts() ) {
        while ( $query->have_posts() ) :
            $query->the_post();

            $output .= '<div class="news_item">';
            // video_video_provider, video_video_id

                $output .= '<a href="' . get_permalink() . '">';
                    $output .= '<h4>' . get_the_title() . '</h4>';
                $output .= '</a>';

                $output .= '<footer>';
                    $output .= '<span class="category"><i class="icon-tag-line"></i> ';
                        $category = get_the_category(get_the_ID());
                        foreach ($category as $item) {
                            $output .= $item->name . ', ';
                        }
                        $output = rtrim($output, ", ");
                    $output .= '</span>';
                    $output .= '<span class="date">' . get_the_date() . '</span>';
                    $output .= '<a class="readmore" href="' . get_permalink() . '"><i class="icon-newspaper"></i>' . esc_html__( 'Read more', 'betheme') . '</a>';
                $output .= '</footer>';

            $output .= '</div>';
        endwhile;
    }
    $output .= '</div>';

    wp_reset_postdata();

    return $output;
}

/* ---------------------------------------------------------------------------
 * Get News post type for sidebar [get_news_sidebar limit="3"]
* --------------------------------------------------------------------------- */
function get_news_sidebar_shortcode( $atts ) {
    // set params
    extract( shortcode_atts( array (
        'limit' => 3
    ), $atts ) );

    // define query parameters based on attributes
    $options = array(
        'post_type'         => 'news',
        'orderby'           => 'date',
        'order'             => 'DESC',
        'posts_per_page'    => $limit,
        'has_password'      => false
    );
    $query = new WP_Query( $options );
    // run the loop based on the query

    $output = '<div class="news_sidebar">';

    if ( $query->have_posts() ) {
        while ( $query->have_posts() ) :
            $query->the_post();

            $output .= '<div class="news_item">';
                // category
                $output .= '<div class="category">';
                    $category = get_the_category(get_the_ID());
                    if ($category) {
                        $output .= '<i class="icon-tag-line"></i>';
                    }
                    foreach ($category as $item) {
                        $output .= $item->name . ', ';
                    }
                    $output = rtrim($output, ", ");
                $output .= '</div>';

                // title
                $output .= '<h4><a href="' . get_permalink() . '">' . get_the_title() . '</a></h4>';

                // date and read more
                $output .= '<footer>';
                    $output .= '<span class="date">' . get_the_date() . '</span>';
                    $output .= '<a class="readmore" href="' . get_permalink() . '"><i class="icon-newspaper"></i>' . esc_html__( 'Read more', 'betheme') .  '</a>';
                $output .= '</footer>';

            $output .= '</div>';
        endwhile;
    }

    $output .= '<a class="readmore" href="' . get_home_url() .  '/news">' . esc_html__('See more', 'betheme') . '</a>';


    $output .= '</div>';

    wp_reset_postdata();

    return $output;
}


/* ---------------------------------------------------------------------------
 * Tabs [tabs][tab][/tab]...[/tabs]
 * --------------------------------------------------------------------------- */
global $tabs_array, $tabs_count;
function sc_tabs( $attr, $content = null )
{
    global $tabs_array, $tabs_count;

    extract(shortcode_atts(array(
        'title'		=> '',
        'tabs'		=> '',
        'type'		=> '',
        'padding'	=> '',
        'uid'		=> '',
    ), $attr));

    do_shortcode( $content );

    // content builder
    if( $tabs ){
        $tabs_array = $tabs;
    }

    // uid
    if( ! $uid ){
        $uid = 'tab-'. uniqid();
    }

    // padding
    if( $padding || $padding === '0' ){
        $padding = 'style="padding:'. esc_attr( $padding ) .'"';
    }

    $output = '';
    if( is_array( $tabs_array ) )
    {
        if( $title ) $output .= '<h4 class="title">'. $title .'</h4>';
        $output .= '<div class="jq-tabs tabs_wrapper tabs_'. $type .'">';

            // contant
            $output .= '<ul>';
                $i = 1;
                $output_tabs = '';
                foreach( $tabs_array as $tab )
                {
                    $output .= '<li><a href="#'. $uid .'-'. $i .'">'. $tab['title'] .'</a></li>';
                    $output_tabs .= '<div id="'. $uid .'-'. $i .'" '. $padding .'>'. do_shortcode( $tab['content'] ) .'</div>';
                    $i++;
                }
            $output .= '</ul>';

            // titles
            $output .= $output_tabs;

        $output .= '</div>';

        $tabs_array = '';
        $tabs_count = 0;
    }

    return $output;
}


/* ---------------------------------------------------------------------------
 * _Tab [tab] _private
 * --------------------------------------------------------------------------- */
$tabs_count = 0;
function sc_tab( $attr, $content = null )
{
    global $tabs_array, $tabs_count;

    extract(shortcode_atts(array(
        'title' => 'Tab title',
    ), $attr));

    $tabs_array[] = array(
        'title' 	=> $title,
        'content' 	=> do_shortcode( $content )
    );
    $tabs_count++;

    return true;
}

/* ---------------------------------------------------------------------------
 * Get Infographic [get_infographic url=""]
* --------------------------------------------------------------------------- */
function get_infographic_shortcode( $attr ) {

    extract(shortcode_atts(array(
        'url'		=> '/qc-en/infographic',
    ), $attr));

    $output = '';
    $output .= '<div class="infographic">';
        $output .= '<iframe class="infographic-iframe" src="' . $url . '" style="width:100%;height:700px;"></iframe>';
        $output .= '<a class="btn" href="' . $url .  '?fullscreen=1">View Infographic</a>';
    $output .= '</div>';
    return $output;
}
