<?php

add_filter( 'excerpt_more', 'mfn_trim_excerpt' );

add_filter( 'tribe-events-bar-filters',  'remove_search_from_bar', 1000, 1 );
remove_action( 'tribe_events_single_event_after_the_content', array( 'Tribe__Events__iCal', 'single_event_links' ) );
remove_filter( 'tribe_events_after_footer', array( 'Tribe__Events__iCal', 'maybe_add_link' ) );

add_filter( 'wpsl_templates', 'rme_wpsl_custom_templates' );
add_filter( 'wpsl_store_meta', 'custom_wpsl_store_meta', 10, 2 );
add_filter( 'wpsl_cpt_info_window_meta_fields', 'custom_wpsl_cpt_info_window_meta_fields', 10, 2 );
add_filter( 'wpsl_listing_template', 'custom_wpsl_listing_template' );
add_filter( 'wpsl_info_window_template', 'custom_wpsl_info_window_template' );
add_filter( 'wpsl_store_header_template', 'custom_wpsl_store_header_template' );
add_filter( 'wpsl_no_results', 'custom_wpsl_no_results' );
add_action( 'wpsl_before_widget_input', 'add_radius_to_wpsl_widget' );

function mfn_trim_excerpt( $more ) {
	return '...';
}

function remove_search_from_bar( $filters ) {
  if ( isset( $filters['tribe-bar-search'] ) ) {
        unset( $filters['tribe-bar-search'] );
    }
 
    return $filters;
}

function rme_wpsl_custom_templates( $templates ) {
    /**
     * The 'id' is for internal use and must be unique ( since 2.0 ).
     * The 'name' is used in the template dropdown on the settings page.
     * The 'path' points to the location of the custom template,
     * in this case the folder of your active theme.
     */
    $templates[] = array (
        'id'   => 'rme-default',
        'name' => 'RME Branded Default',
        'path' => get_stylesheet_directory() . '/includes/wpsl-rme-template.php',
    );

    return $templates; 
}

function custom_wpsl_store_meta( $store_meta, $store_id ) {
    $terms = wp_get_post_terms( $store_id, 'wpsl_store_category' );
    $store_meta['terms'] = '';
    
    if ( $terms ) {
        if ( !is_wp_error( $terms ) ) {
            if ( count( $terms ) > 1 ) {
                $location_terms = array();

                foreach ( $terms as $term ) {
                    $location_terms[] = $term->name;
                }

                $store_meta['terms'] = implode( ', ', $location_terms );
            } else {
                $store_meta['terms'] = $terms[0]->name;    
            }
        }
    }
    
    if ( function_exists( 'z_taxonomy_image_url' ) ) {
        $terms = wp_get_post_terms( $store_id, 'wpsl_store_category' );

        if ( $terms ) {
            if ( !is_wp_error( $terms ) ) {
                if ( isset( $_GET['filter'] ) && $_GET['filter'] ) {
                    $filter_ids = explode( ',', $_GET['filter'] );

                    foreach ( $terms as $term ) {
                        if ( in_array( $term->term_id, $filter_ids ) ) {
                            $cat_marker = z_taxonomy_image_url( $term->term_id );

                            if ( $cat_marker ) {
                                $store_meta['categoryMarkerUrl'] = $cat_marker;
                            }
                        }
                    }
                } else {
                    $store_meta['categoryMarkerUrl'] = z_taxonomy_image_url( $terms[0]->term_id );
                }
            }
        }
    }

    return $store_meta;
}


function custom_wpsl_cpt_info_window_meta_fields( $meta_fields, $store_id ) {

    $terms = wp_get_post_terms( $store_id, 'wpsl_store_category' );

    if ( $terms ) {
        if ( !is_wp_error( $terms ) ) {
            if ( function_exists( 'z_taxonomy_image_url' ) ) {
                $meta_fields['categoryMarkerUrl'] = z_taxonomy_image_url( $terms[0]->term_id );
            }
        }
    }

    return $meta_fields;
}

function custom_wpsl_store_header_template( $header_template ) {
    global $wpsl_settings;

    if ( $wpsl_settings['new_window'] ) {
        $new_window = ' target="_blank" ';
    } else {
        $new_window = '';
    }

    if ( $wpsl_settings['permalinks'] ) {
        $header_template = '<% if ( typeof permalink !== "undefined" ) { %>' . "\r\n";
        $header_template .= '<span class="wpsl-name" itemprop="name"><a' . $new_window . 'href="<%= permalink %>"><%= store %></a></span>' . "\r\n";
        $header_template .= '<% } else { %>' . "\r\n";
        $header_template .= '<span class="wpsl-name" itemprop="name"><%= store %></span>' . "\r\n";
        $header_template .= '<% } %>'; 
    } else {
        $header_template = '<% if ( wpslSettings.storeUrl == 1 && url ) { %>' . "\r\n";
        $header_template .= $tab . '<span class="wpsl-name" itemprop="name"><a' . $new_window . ' href="<%= url %>"><%= store %></a></span>' . "\r\n";
        $header_template .= $tab . '<% } else { %>' . "\r\n";
        $header_template .= $tab . '<span class="wpsl-name" itemprop="name"><%= store %></span>' . "\r\n";
        $header_template .= $tab . '<% } %>'; 
    }
        
    return $header_template;
}

function custom_wpsl_listing_template() {
    global $wpsl, $wpsl_settings;

    $listing_template = '<li data-store-id="<%= id %>" class="list-content" itemscope itemtype="http://schema.org/RecyclingCenter">' . "\r\n";
   
    $listing_template .= "\t\t" . '<div class="wpsl-result-number">' . "\r\n";
    $listing_template .= "\t\t\t" . '<%= resultNumber %>' . "\r\n";
    $listing_template .= "\t\t" . '</div>' . "\r\n";

    $listing_template .= "\t\t" . '<div class="wpsl-store-location">' . "\r\n";
    $listing_template .= "\t\t\t" . '<span itemprop="priceRange" hidden>Free</span>' . "\r\n";
    $listing_template .= "\t\t\t" . '<span itemprop="description" hidden>Electronics recycling facility for cellphones, computers and anything else electronic.</span>' . "\r\n";
    $listing_template .= "\t\t\t" . '<p><span itemprop="image" itemscope itemtype="http://schema.org/ImageObject"><%= thumb %></span>' . "\r\n";
    $listing_template .= "\t\t\t\t" . wpsl_store_header_template( 'listing' ) . "\r\n"; // Check which header format we use

     // Include the category names.
    $listing_template .= "\t\t\t" . '<% if ( terms ) { %>' . "\r\n";
    $listing_template .= "\t\t\t" . '<span class="wpsl-cat"><%= terms %></span>' . "\r\n";
    $listing_template .= "\t\t\t" . '<% } %>' . "\r\n";

    $listing_template .= "\t\t\t\t" . '<span class="wpsl-street" itemprop="streetAddress"><%= address %></span>' . "\r\n";
    $listing_template .= "\t\t\t\t" . '<% if ( address2 ) { %>' . "\r\n";
    $listing_template .= "\t\t\t\t" . '<span class="wpsl-street" itemprop="streetAddress"><%= address2 %></span>' . "\r\n";
    $listing_template .= "\t\t\t\t" . '<% } %>' . "\r\n";
    $listing_template .= "\t\t\t\t" . '<span itemprop="addressLocality"><%= city %>, </span><span itemprop="addressRegion"><%= state%> </span><span itemprop="postalCode"><%= zip %></span>' . "\r\n"; // Use the correct address format

    if ( !$wpsl_settings['hide_country'] ) {
        $listing_template .= "\t\t\t\t" . '<span class="wpsl-country" itemprop="addressLocality"><%= country %></span>' . "\r\n";
    }

    $listing_template .= "\t\t\t" . '</p>' . "\r\n";

    // Show the phone, fax or email data if they exist.
    if ( $wpsl_settings['show_contact_details'] ) {
        $listing_template .= "\t\t\t" . '<p class="wpsl-contact-details">' . "\r\n";
        $listing_template .= "\t\t\t" . '<% if ( phone ) { %>' . "\r\n";
        $listing_template .= "\t\t\t" . '<span itemprop="telephone"><%= formatPhoneNumber( phone ) %></span>' . "\r\n";
        $listing_template .= "\t\t\t" . '<% } %>' . "\r\n";
        $listing_template .= "\t\t\t" . '<% if ( fax ) { %>' . "\r\n";
        $listing_template .= "\t\t\t" . '<span><strong>' . esc_html( $wpsl->i18n->get_translation( 'fax_label', __( 'Fax', 'wpsl' ) ) ) . '</strong>: <%= fax %></span>' . "\r\n";
        $listing_template .= "\t\t\t" . '<% } %>' . "\r\n";
        $listing_template .= "\t\t\t" . '<% if ( email ) { %>' . "\r\n";
        $listing_template .= "\t\t\t" . '<span><strong>' . esc_html( $wpsl->i18n->get_translation( 'email_label', __( 'Email', 'wpsl' ) ) ) . '</strong>: <%= email %></span>' . "\r\n";
        $listing_template .= "\t\t\t" . '<% } %>' . "\r\n";
        $listing_template .= "\t\t\t" . '<% if ( url ) { %>' . "\r\n";
        $listing_template .= "\t\t\t" . '<a href="<%= url %>" target="_blank" title="<%= store %>"><%= url %></a>' . "\r\n";
        $listing_template .= "\t\t\t" . '<% } %>' . "\r\n";
        $listing_template .= "\t\t\t" . '</p>' . "\r\n";
    }

    $listing_template .= "\t\t\t" . wpsl_more_info_template() . "\r\n"; // Check if we need to show the 'More Info' link and info
    $listing_template .= "\t\t" . '<div class="wpsl-direction-wrap">' . "\r\n";

    if ( !$wpsl_settings['hide_distance'] ) {
        $listing_template .= "\t\t\t" . '<%= distance %> ' . esc_html( $wpsl_settings['distance_unit'] ) . '' . "\r\n";
    }

    $listing_template .= "\t\t\t" . '<%= createDirectionUrl() %>' . "\r\n"; 
    $listing_template .= "\t\t" . '</div>' . "\r\n";
    $listing_template .= "\t\t" . '</div>' . "\r\n";
    $listing_template .= "\t" . '</li>';

    return $listing_template;
}

function custom_wpsl_info_window_template() {
    $info_window_template = '<div data-store-id="<%= id %>" class="wpsl-info-window">' . "\r\n";
    $info_window_template .= "\t\t" . '<p>' . "\r\n";
    $info_window_template .= "\t\t" . '<span class="wpsl-result-number"><%= resultNumber %></span>' . "\r\n";
    $info_window_template .= "\t\t\t" .  wpsl_store_header_template() . "\r\n";  
    $info_window_template .= "\t\t\t" . '<span><%= address %></span>' . "\r\n";
    $info_window_template .= "\t\t\t" . '<% if ( address2 ) { %>' . "\r\n";
    $info_window_template .= "\t\t\t" . '<span><%= address2 %></span>' . "\r\n";
    $info_window_template .= "\t\t\t" . '<% } %>' . "\r\n";
    $info_window_template .= "\t\t\t" . '<span>' . wpsl_address_format_placeholders() . '</span>' . "\r\n";
    $info_window_template .= "\t\t" . '</p>' . "\r\n";
    
    // Include the category names.
    $info_window_template .= "\t\t" . '<% if ( terms ) { %>' . "\r\n";
    $info_window_template .= "\t\t" . '<p><%= terms %></p>' . "\r\n";
    $info_window_template .= "\t\t" . '<% } %>' . "\r\n";
    
    $info_window_template .= "\t\t" . '<%= createInfoWindowActions( id ) %>' . "\r\n";
    $info_window_template .= "\t" . '</div>' . "\r\n";
    
    return $info_window_template;
}

function custom_wpsl_no_results() {
    $output = '<h4>No results found!</h4>';
    return $output;
}

function add_radius_to_wpsl_widget() {
    global $wpsl_settings;

    $dropdown_list = '';
    $settings      = explode( ',', $wpsl_settings['search_radius'] );
    
    // Only show the distance unit if we are dealing with the search radius.
    $distance_unit = ' '. esc_attr( wpsl_get_distance_unit() );

    foreach ( $settings as $index => $setting_value ) {
        // The default radius has a [] wrapped around it, so we check for that and filter out the [].
        if ( strpos( $setting_value, '[' ) !== false ) {
            $setting_value = filter_var( $setting_value, FILTER_SANITIZE_NUMBER_INT );
            $selected = 'selected="selected" ';
        } else {
            $selected = '';
        }	

        $dropdown_list .= '<option ' . $selected . 'value="'. absint( $setting_value ) .'">'. absint( $setting_value ) . $distance_unit .'</option>';
    }	
    
    echo '<p>';
    echo    '<label for="wpsl-widget-radius">' . __( 'within', 'wpsl' ) . '</label>';
    echo    '<select id="wpsl-widget-radius" class="wpsl-dropdown" name="wpsl-widget-radius">';
    echo        $dropdown_list;
    echo    '</select>';
    echo '</p>';
}

add_filter( 'wpsl_store_data', 'add_result_number_to_wpsl' );
function add_result_number_to_wpsl( $store_data ) {
    $i = 1;

    foreach ( $store_data as $k => $store ) {
        $store_data[ $k ]['resultNumber'] = $i++;
    }

    return $store_data;
}