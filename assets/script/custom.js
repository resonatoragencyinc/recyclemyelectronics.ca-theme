jQuery(document).ready(function($) {

	lazyload();

	//set up breadcrumb
	$('#Subheader ul.breadcrumbs li span').html('::');

	// setup footer search
	$('#Footer .widget_search .icon_search').removeClass('icon-search-fine');
	$('#Footer .widget_search .icon_search').addClass('icon-search');

	// remove margin for no image header
	var headerBg = $('body:not(.template-slider) #Header_wrapper').css('background-image');
	if (headerBg === 'none') {
		$('#Subheader').css('margin-top', '40px')
	}


	// calculate
	$('.calculator-wrap button').click(function () {
		console.log('button');
		var laptop		= $('#input_laptop').val();
		var desktop 	= $('#input_desktop').val();
		var tv			= $('#input_tv').val();
		var led 		= $('#input_led').val();
		var portable 	= $('#input_portable').val();

		var tonnage = laptop + desktop + led + portable;

		$('.tonnage .result').html(tonnage);

		var result_gold = laptop * gold[0] +
		$('.gold').append();
		$('.silver').append(tonnage * silver);
		$('.copper').append(tonnage * copper);
		$('.palladium').append(tonnage * palladium);
	});

	var input = document.getElementById('bh-sl-address-widget');

	if ( !jQuery.isEmptyObject(input) ) {
		var autocomplete = new google.maps.places.Autocomplete(input);
			autocomplete.addListener('place_changed', function () {
			_placeChanged(autocomplete, input);
		});

		var _placeChanged = function (autocomplete, input) {
			var place = autocomplete.getPlace();
			_updateAddress({
				input: input,
				address_components: place.address_components
			});
		}
	}

	var language = $("meta[property='og:locale']").attr("content");
	if ( language == 'fr_CA' ) {
		$('#bh-sl-address').attr('placeholder', 'Code postal ou ville');
	} else {
		$('#bh-sl-address').attr('placeholder', 'Postal Code or City');
	}

	// map page
	if ( $('.legend-box')[0] ) {
		$('body').on('click', '.bh-sl-loc-list li', function(){
			var map_top = $('#bh-sl-map-container').offset().top;
			$('html, body').animate({
        scrollTop: map_top
      }, 500);
		})
	}

	if ( $('#wpsl-stores')[0] ) {
		$('#wpsl-stores').on('click', '.wpsl-store-details', function() {
			var map_top = $('#wpsl-gmap').offset().top;
			$('html, body').animate({
        scrollTop: map_top
      }, 500);
		})
	}

});

// Cardinal locator default geocode no result
function cslNoResults(map, myOptions) {
  map.setZoom(12);
}

function cslNotify(notifyText) {
  console.log(notifyText);
  return false;
}
